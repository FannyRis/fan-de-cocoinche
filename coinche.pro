QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += console

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += include/

HEADERS       = include/Belote.h \
                include/Bid.h \
                include/Card.h \
                include/Client.h \
                include/Constants.h \
                include/Deck.h \
                include/Player.h \
                include/Scores.h \
                include/qt/ClientGUI.h \
                include/qt/GUIPositionWindow.h \
                include/qt/QClickableLabel.h \
                lib/Socket/TCP/basesocket.h \
                lib/Socket/TCP/tcpserversocket.h \
                lib/Socket/TCP/tcpsocket.h \
                lib/Socket/socketexception.h

SOURCES       = src/main_client.cpp \
                src/Deck.cc \
                src/Client.cc \
                src/Scores.cc \
                src/Player.cc \
                src/qt/ClientGUI.cc \
                src/qt/QClickableLabel.cc \
                src/qt/GUIPositionWindow.cc \
                lib/Socket/TCP/basesocket.cpp \
                lib/Socket/TCP/tcpserversocket.cpp \
                lib/Socket/TCP/tcpsocket.cpp \
                lib/Socket/socketexception.cpp

QT           += widgets

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += images.qrc
FORMS += include/qt/client.ui
