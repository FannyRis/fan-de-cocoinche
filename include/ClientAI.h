////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief             Client AI for the coinche game.
///
/// \copyright         C&F
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_CLIENTAI_H
#define FAN_DE_COCOINCHE_CLIENTAI_H

#include "Bid.h"
#include "Client.h"
#include "Player.h"
#include "Scores.h"

///////////////////////////////////////////////////////////////////////////////
///
/// \brief Client for the coinAIche game.
///
///////////////////////////////////////////////////////////////////////////////
class ClientAI : public Client
{
public:
    ///////////////////////////////////////////////////////////////////////////
    /// \brief      Constructor.
    ////////////////////////////////////////////////////////////////////////////////////////
    ClientAI(string const& name, string const& server = DEFAULT_SERVER);

    ///////////////////////////////////////////////////////////////////////////
    /// \brief      Destructor.
    ///////////////////////////////////////////////////////////////////////////
    ~ClientAI() = default;

private:
    void positionCallback() override;
    void auctionCallback(string const& msg) override;
    void playCallback(string const& msg) override;

    void welcomeCallback(string const& msg) override;
    void newPlayerCallback(string const& msg) override;
    void startCallback() override;
    void handCallback(string const& msg) override;
    void indexCallback(string const& msg) override;
    void lastBidCallback(string const& msg) override;
    void lastCardCallback(string const& msg) override;
    void trickCallback(string const& msg) override;
    void contractCallback(string const& msg) override;
    void gamePointsCallback(string const& msg) override;
    void scoresCallback(string const& msg, bool const& isEnd = false) override;

    void coincheCallback(string const& msg) override;
    void beloteCallback() override;

    void tryAgainCallback(string const& msg = "") override;
    void genericErrorCallback(string const& msg = "") override;

    vector<int> getIndexes() const;
    Bid evaluateHand();
    Bid evaluateOn(Bid const& otherBid);
    Bid getLastValidBid();

    Bid doBid();
    uint16_t playCard();

    bool isWinning(Card const& card) const;

    Bid contract_;
    bool hasBelote_;
    std::vector<Bid> bids_;
    Deck cards_;
    std::vector<bool> mask_;

    Bid lastBid_;
    Deck playedCards_;
};

#endif