////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief             Client for the coinche game.
///
/// \copyright         C&F
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_CLIENT_H
#define FAN_DE_COCOINCHE_CLIENT_H

#include <deque>

#include "Bid.h"
#include "Constants.h"
#include "Deck.h"
#include "Player.h"
#include "Scores.h"

#include "../lib/Socket/TCP/tcpsocket.h"

////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Client for the coinche game.
///
////////////////////////////////////////////////////////////////////////////////////////////////
class Client
{
public:
    ////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Constructor.
    ////////////////////////////////////////////////////////////////////////////////////////
    Client(string const& name, string const& server = DEFAULT_SERVER);

    ////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Destructor.
    ////////////////////////////////////////////////////////////////////////////////////////
    ~Client();

    ////////////////////////////////////////////////////////////////////////////////////////
    /// \brief
    ////////////////////////////////////////////////////////////////////////////////////////
    void joinWriteThread();
    void waitForEnd();
    void logout();

    friend void* doRead(Client* clientIn);
    friend void* doWrite(Client* clientIn);

protected:
    void* readLoop();
    void* writeLoop();

    // Callbacks
    virtual void positionCallback();
    virtual void auctionCallback(string const& msg);
    virtual void playCallback(string const& msg);

    virtual void welcomeCallback(string const& msg);
    virtual void newPlayerCallback(string const& msg);
    virtual void startCallback();
    virtual void handCallback(string const& msg);
    virtual void indexCallback(string const& msg);
    virtual void lastBidCallback(string const& msg);
    virtual void lastCardCallback(string const& msg);
    virtual void trickCallback(string const& msg);
    virtual void contractCallback(string const& msg);
    virtual void gamePointsCallback(string const& msg);
    virtual void scoresCallback(string const& msg, bool const& isEnd = false);

    virtual void coincheCallback(string const& msg);
    virtual void beloteCallback();
    void exitCallback();

    virtual void tryAgainCallback(string const& msg = "");
    virtual void genericErrorCallback(string const& msg = "");

    string availablePositionsToString() const;
    string toBidRegex(string const& stringIn) const;
    vector<bool> stringToMask(string const& stringIn);

    bool run_signal_;

    PlayerId id_;
    array<Player, 4> players_;
    Bid contract_;
    Deck hand_;
    Scores scores_;
    bool belote_;

    TCPSocket client_socket_;
    string server_;

    bool waitingForPosition_;
    bool waitingForAuction_;
    bool waitingForCoinche_;
    bool waitingForPlay_;

    std::thread readThread_;
};

#endif
