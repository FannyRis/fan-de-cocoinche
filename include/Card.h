////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Card structure
///
/// \copyright	C&F
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_CARD_H
#define FAN_DE_COCOINCHE_CARD_H

#include "Constants.h"


struct Card
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Default empty constructor.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Card()
        : color(N_COLORS)
        , figure(N_FIGURES)
    {
        // empty
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Normal constructors.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Card(Color colorIn, Figure figureIn)
        : color(colorIn)
        , figure(figureIn)
    {
        // empty
    }

    Card(Figure figureIn, Color colorIn)
        : color(colorIn)
        , figure(figureIn)
    {
        // empty
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      String constructor.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Card(std::string const& stringIn)
        : color(N_COLORS)
        , figure(N_FIGURES)
    {
        if (regex_match(stringIn, std::regex(CARD_REGEX)))
        {
            color = stringToColor(stringIn.substr(0, 1));
            figure = stringToFigure(stringIn.substr(1));
        }
    }

    Color color;
    Figure figure;

    uint32_t getTrumpValue() const
    {
        return TRUMP_VALUES[figure];
    }

    uint32_t getNoTrumpValue() const
    {
        return NO_TRUMP_VALUES[figure];
    }

    bool operator==(Card const& other) const
    {
        return this->color == other.color && this->figure == other.figure;
    }

    bool operator<(Card const& other) const
    {
        return this->figure < other.figure;
    }

    bool operator>(Card const& other) const
    {
        return this->figure > other.figure;
    }

    bool operator<=(Card const& other) const
    {
        return this->figure <= other.figure;
    }

    bool operator>=(Card const& other) const
    {
        return this->figure >= other.figure;
    }


    static bool smallerTrump(Card const& c1, Card const& c2)
    {
        int indexThis = 0;
        int indexOther = 0;
        for (uint32_t i = 0; i < 8; i++)
        {
            if (ORDER_TRUMP[i] == c1.figure)
            {
                indexThis = i;
            }
            if (ORDER_TRUMP[i] == c2.figure)
            {
                indexOther = i;
            }
        }
        return indexThis < indexOther;
    }


    bool smallerThan(Card const& other, Color const trump, Color const askedColor) const
    {
        if (color == trump and other.color == trump)
        {
            int indexThis = 0;
            int indexOther = 0;
            for (uint32_t i = 0; i < 8; i++)
            {
                if (ORDER_TRUMP[i] == figure)
                {
                    indexThis = i;
                }
                if (ORDER_TRUMP[i] == other.figure)
                {
                    indexOther = i;
                }
            }
            return indexThis < indexOther;
        }
        else if (color == trump)
        {
            return false;
        }
        else if (other.color == trump)
        {
            return true;
        }
        else if (color == askedColor and other.color == askedColor)
        {
            return figure < other.figure;
        }
        else if (color == askedColor)
        {
            return false;
        }
        else if (other.color == askedColor)
        {
            return true;
        }
        return false;
    }


    string toPath() const
    {
        return to_string(color) + to_string(figure) + ".png";
    }


    std::string toPrettyIcon() const
    {
        return figureIcon[figure] + colorIcon[color];
    }


    std::string toPrettyString() const
    {
        return figureStrings[figure] + " of " + colorStrings[color];
    }

    std::string toPaddedPrettyString() const
    {
        return padString(figureStrings[figure], FIGURE_LENGTH) + " of " + colorStrings[color];
    }

    std::string toString() const
    {
        return to_string(color) + to_string(figure);
    }
};

#endif