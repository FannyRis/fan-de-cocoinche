////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief             Motor.
///
/// \copyright         C&F
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_MOTOR_H
#define FAN_DE_COCOINCHE_MOTOR_H

#include "Bid.h"
#include "Deck.h"
#include "Player.h"
#include "Scores.h"
#include "Belote.h"

#include "signal.h"

#include "../lib/Socket/TCP/tcpserversocket.h"

////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Motor class
///
////////////////////////////////////////////////////////////////////////////////////////////////
class Motor
{
public:
    Motor();

    ////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Destructor.
    ////////////////////////////////////////////////////////////////////////////////////////
    ~Motor();

    void run(uint32_t nAI);

private:    
    void sendAllClients(string const& tag, string const& msg = "");
    void sendClient(PlayerId const& player, string const& tag, string const& msg = "");
    Message getFirstMessage();

    void acceptClient();
    PlayerId getPosition(PlayerId const& player);
    void deal();
    void runAuctions();
    Bid getAuction(PlayerId player);

    void playTrick();
    Card getCard(PlayerId const& player, vector<bool> const& handMask);

    bool isBidValid(Bid const& bid) const;
    bool isCoincheValid(PlayerId const& player) const;
    bool isBeloteValid(PlayerId const& player, Card const& card) const;

    vector<bool> getHandMask(Deck const& cards) const;
    bool isPartnerWinning(Deck const& cards) const;
    Card getHighestTrump(Deck const& cards) const;
    bool isCardAllowed(Deck const& cards, Deck const& hand, int const& index) const;

    void computeScores();
    string maskToString(vector<bool> const& mask);
    string playersToString();
    PlayerId clientToPlayerId(TCPSocket const& client) const;
    Player getPlayer(PlayerId const& idIn) const;
    vector<PlayerId> getAvailablePositions() const;

    PlayerId currentPlayer_;
    PlayerId firstPlayer_;
    PlayerId firstTrickPlayer_;

    Scores scores_;

    Deck deck_;
    array<Deck, 2> teamDecks_;
    array<Player, 4> players_;

    TeamId declarerTeam_;
    Bid contract_;
    uint32_t numberOfPass_;
    bool hasValidBid_;
    bool stopAuctions_;

    uint32_t numberOfTrick_;
    Belote belote_;

    State state_;

    TCPServerSocket server_socket_;
    mutex queue_mutex_;
    vector<pid_t> AIpids_;
};

#endif
