////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Constants
///
/// \copyright	C&F
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_CONSTANTS_H
#define FAN_DE_COCOINCHE_CONSTANTS_H

#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <regex>
#include <vector>
#include <thread>
#include <mutex>

using namespace std;

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Tags
////////////////////////////////////////////////////////////////////////////////////////////////////
// Action ags
#define POSITION_TAG "00"
#define AUCTION_TAG "01"
#define PLAY_TAG "02"

// Info tags
#define WELCOME_TAG "09"
#define NEW_PLAYER_TAG "10"
#define ALL_PLAYERS_CONNECTED_TAG "11"
#define HAND_TAG "12"
#define INDEX_HAND_TAG "13"
#define LAST_BID_TAG "14"
#define LAST_CARD_TAG "15"
#define TRICK_TAG "16"
#define CONTRACT_TAG "17"
#define GAME_POINTS_TAG "18"
#define SCORES_TAG "19"

// Answer Tags
#define COINCHE_TAG "31"
#define BELOTE_TAG "32"

// Errors
#define INVALID_POSITION_TAG "79"
#define INVALID_AUCTION_MESSAGE_TAG "80"
#define INVALID_BID_TAG "81"
#define INVALID_COINCHE_TAG "82"
#define INVALID_CARD_TAG "83"
#define INVALID_BELOTE_TAG "84"
#define INCOMPLETE_BELOTE_TAG "85"

// trigger tag
#define END_TAG "98"
#define EXIT_TAG "99"


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Regex
////////////////////////////////////////////////////////////////////////////////////////////////////
#define POSITION_REGEX "[NSEW]{1}"
#define PLAYERS_REGEX "([NSEW]{1}[a-zA-Z0-9-_ ]*)*"
#define PLAY_REGEX "[0-7]{1}[B]{0,1}"
#define CARD_REGEX "[HCDS ]{1}[7891JQKA ]{1}"
#define BID_REGEX "[12]{1}[NESW ]{1}(([HCDS ]{1}[0-9]{2,3})|P)"
#define CLIENT_BID_REGEX "pass|([0-9]{2,3}-[HCDS])"
#define COINCHE_REGEX "(toctoc)|(coinche)"
#define DECK_REGEX "([HCDS ]{1}[7891JQKA]{1})*"
#define SCORE_REGEX "[0-9]{1,4}-[0-9]{1,4}"
#define EXIT_REGEX "exit|x|quit|q"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Network constants and default sizes
////////////////////////////////////////////////////////////////////////////////////////////////////
#define DEFAULT_PORT "5555"
#define DEFAULT_SERVER "127.0.1.1"
#define OUT_BUFFER_SIZE 4096
#define SLEEP_DELAY 100
#define MEDIUM_SLEEP_DELAY 5000
#define LONG_SLEEP_DELAY 500000

#define NAME_LENGTH 5
#define SCORE_LENGTH 4
#define FIGURE_LENGTH 5
#define MINIMUM_CARD_TEXT_SIZE 10
#define PASS_SIZE 4
#define BID_HEADER_SIZE 14
#define PLAY_HEADER_SIZE 14


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Coinche constants
////////////////////////////////////////////////////////////////////////////////////////////////////
uint16_t const MAX_SCORE = 500;


////////////////////////////////////////////////////////////////////////////////////////////////////
/// GUI constants
////////////////////////////////////////////////////////////////////////////////////////////////////
#define CARD_PATH ":/cards/images/cards/"
#define BACKGROUND_PATH ":/misc/images/"


vector<uint32_t> const NO_TRUMP_VALUES = {0, 0, 0, 2, 3, 4, 10, 11};
vector<uint32_t> const TRUMP_VALUES = {0, 0, 14, 20, 3, 4, 10, 11};

vector<uint32_t> const AUTHORIZED_VALUES = {80, 90, 100, 110, 120, 130, 140,
                                            150, 160, 170, 180, 250, 270, 500, 520};


static string padString(string stringIn, int const& length, bool const& leftPadding = false)
{
    string string = stringIn;
    if (string.size() < length)
    {
        while (string.size() != length)
        {
            if (leftPadding)
            {
                string = " " + string;
            }
            else
            {
                string = string + " ";
            }
        }
    }
    else if (string.size() > length)
    {
        string = string.substr(0, length);
    }
    return string;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Player
////////////////////////////////////////////////////////////////////////////////////////////////////
enum PlayerId
{
    SOUTH,
    WEST,
    NORTH,
    EAST,
    N_PLAYERS
};

vector<string> const playerIdStrings = { "South", "West", "North", "East", " "};

static string to_string(PlayerId const player)
{
    return playerIdStrings[player].substr(0, 1);
}

static PlayerId stringToPlayerId(string const& stringIn)
{
    for (int e = 0; e != N_PLAYERS; e++)
    {
        PlayerId player = static_cast<PlayerId>(e);
        if (to_string(player).substr(0, 1) == stringIn)
        {
            return player;
        }
    }
    return N_PLAYERS;
}

static PlayerId prettySringToPlayerId(string const& stringIn)
{
    return stringToPlayerId(stringIn.substr(0, 1));
}

static PlayerId operator++(PlayerId& player, int)
{
    switch (player)
    {
    case SOUTH:
    case WEST:
    case NORTH:
        player = static_cast<PlayerId>(player + 1);
        break;
    case EAST:
        player = SOUTH;
        break;
    default:
        player = N_PLAYERS;
    }
    return player;
}

static PlayerId& operator+=(PlayerId& player, const int& add)
{
    for (int i = 0; i < add; i++)
    {
        player++;
    }
    return player;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Team Id
////////////////////////////////////////////////////////////////////////////////////////////////////
enum TeamId
{
    NORTH_SOUTH,
    EAST_WEST,
    N_TEAMS
};

vector<string> const teamIdStrings = {"North-South", "East-West", " "};
vector<TeamId> const playersTeam = {NORTH_SOUTH, EAST_WEST, NORTH_SOUTH, EAST_WEST, N_TEAMS};

static string to_string(TeamId const teamId)
{
    return teamIdStrings[teamId].substr(0, 1);
}

static TeamId stringToTeamId(string const& stringIn)
{
    for (int e = 0; e != N_TEAMS; e++)
    {
        TeamId teamId = static_cast<TeamId>(e);
        if (to_string(teamId).substr(0, 1) == stringIn)
        {
            return teamId;
        }
    }
    return N_TEAMS;
}

static TeamId prettySringToTeamId(string const& stringIn)
{
    return stringToTeamId(stringIn.substr(0, 1));
}

static TeamId operator++(TeamId& team, int)
{
    switch (team)
    {
    case NORTH_SOUTH:
        team = EAST_WEST;
        break;
    case EAST_WEST:
        team = NORTH_SOUTH;
        break;
    default:
        team = N_TEAMS;
    }
    return team;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Color
////////////////////////////////////////////////////////////////////////////////////////////////////
enum Color
{
    HEARTS,
    CLUBS,
    DIAMONDS,
    SPADES,
    N_COLORS
};

vector<string> const colorStrings = {"Hearts", "Clubs", "Diamonds", "Spades", " "};
vector<string> const colorIcon = {"\xE2\x99\xA5", "\xE2\x99\xA3", "\xE2\x99\xA6", "\xE2\x99\xA0", " "};

static string to_string(Color const colorIn)
{
    return colorStrings[colorIn].substr(0, 1);
}

static Color stringToColor(string const& stringIn)
{
    for (int e = 0; e != N_COLORS; e++)
    {
        Color color = static_cast<Color>(e);
        if (to_string(color).substr(0, 1) == stringIn)
        {
            return color;
        }
    }
    return N_COLORS;
}

static Color prettySringToColor(string const& stringIn)
{
    return stringToColor(stringIn.substr(0, 1));
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Figure
////////////////////////////////////////////////////////////////////////////////////////////////////
enum Figure
{
    SEVEN,
    EIGHT,
    NINE,
    JACK,
    QUEEN,
    KING,
    TEN,
    ACE,
    N_FIGURES
};

vector<string> const figureStrings = {"7", "8", "9", "Jack", "Queen", "King", "10", "Ace", " "};
vector<string> const figureIcon = {"7", "8", "9", "V", "D", "R", "10", "As", " "};

static string to_string(Figure const figureIn)
{
    return figureStrings[figureIn].substr(0, 1);
}

static Figure stringToFigure(string const& stringIn)
{
    for (int e = 0; e != N_FIGURES; e++)
    {
        Figure figure = static_cast<Figure>(e);
        if (to_string(figure).substr(0, 1) == stringIn)
        {
            return figure;
        }
    }
    return N_FIGURES;
}

static Figure prettyStringToFigure(string const& stringIn)
{
    return stringToFigure(stringIn.substr(0, 1));
}

vector<Figure> const ORDER_TRUMP = {SEVEN, EIGHT, QUEEN, KING, TEN, ACE, NINE, JACK};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// State
////////////////////////////////////////////////////////////////////////////////////////////////////
enum State
{
    WAIT_FOR_PLAYERS,
    DEAL,
    AUCTIONS,
    PLAY,
    END
};

vector<string> const stateStrings = {"Wait for players", "Deal", "Auctions", "Play", "End"};


////////////////////////////////////////////////////////////////////////////////////////////////////
/// Message struct
////////////////////////////////////////////////////////////////////////////////////////////////////
struct Message
{
    PlayerId sender;
    string content;
};

#endif