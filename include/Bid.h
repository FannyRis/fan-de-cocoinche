////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Bid structure
///
/// \copyright	C&F
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_BID_H
#define FAN_DE_COCOINCHE_BID_H

#include "Constants.h"

struct Bid
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Default empty constructor.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Bid()
        : value(0)
        , color(N_COLORS)
        , pass(true)
        , player(N_PLAYERS)
        , coincheCoefficient(1)
    {
        // empty
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Pass constructor.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Bid(PlayerId const& playerIn)
        : value(0)
        , color(N_COLORS)
        , pass(true)
        , player(playerIn)
        , coincheCoefficient(1)
    {
        // empty
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      No-pass constructor
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Bid(uint32_t const valueIn, Color const colorIn, PlayerId const& playerIn, uint8_t const& coinche = 1)
        : value(valueIn)
        , color(colorIn)
        , pass(false)
        , player(playerIn)
        , coincheCoefficient(coinche)
    {
        // empty
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      Copy - coinche constructor
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Bid(Bid const& other, uint8_t const& coinche = 1)
        : value(other.value)
        , color(other.color)
        , pass(other.pass)
        , player(other.player)
        , coincheCoefficient(coinche)
    {
        // empty
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief      String constructor
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Bid(std::string const& stringIn)
        : value(0)
        , color(N_COLORS)
        , pass(true)
        , player(N_PLAYERS)
        , coincheCoefficient(1)
    {
        if (regex_match(stringIn, std::regex(BID_REGEX)))
        {
            coincheCoefficient = atoi(stringIn.substr(0, 1).c_str());
            player = stringToPlayerId(stringIn.substr(1, 1));
            if (stringIn.substr(2, 1) != "P")
            {
                pass = false;
                color = stringToColor(stringIn.substr(2, 1));
                value = atoi(stringIn.substr(3).c_str());
            }
        }
    }


    bool isValid() const
    {
        if (player == N_PLAYERS)
        {
            return false;
        }

        if (pass == true)
        {
            return true;
        }

        auto it = std::find(AUTHORIZED_VALUES.begin(), AUTHORIZED_VALUES.end(), value);
        if (it != AUTHORIZED_VALUES.end())
        {
            return true;
        }
        return false;
    }


    bool isValid(Bid const& lastBid) const
    {
        if (player == N_PLAYERS)
        {
            return false;
        }

        if (pass == true)
        {
            return true;
        }
        return isValid() and value > lastBid.value;
    }


    std::string toPrettyString()
    {
        if (player == N_PLAYERS)
        {
            return "No bid";
        }

        if (pass == true)
        {
            return " passed";
        }

        return " said " + std::to_string(value) + " " + colorStrings[color];
    }


    std::string toString()
    {
        if (player == N_PLAYERS)
        {
            return " ";
        }

        if (pass == true)
        {
            return "1" + to_string(player) + "P";
        }

        return std::to_string(coincheCoefficient) + to_string(player) + to_string(color) + std::to_string(value);
    }


    std::string contractString()
    {
        std::string result = "Declarer: " + teamIdStrings[playersTeam[player]] + "\n";
        result += std::to_string(value) + " at " + colorStrings[color] + "\n";
        if (coincheCoefficient > 1)
        {
            result += "Coinché ! \n";
        }

        return result;
    }


    PlayerId player;
    uint32_t value;
    Color color;
    bool pass;
    uint8_t coincheCoefficient;
};

#endif