////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief             Client GUI for the coinche game.
///
/// \copyright         C&F
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef FAN_DE_COCOINCHE_CLIENTGUI_H
#define FAN_DE_COCOINCHE_CLIENTGUI_H

#include "Bid.h"
#include "Client.h"
#include "Player.h"
#include "Scores.h"

#include <QWidget>
#include <QLabel>
#include "QClickableLabel.h"
#include "GUIPositionWindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class ClientGUI; }
QT_END_NAMESPACE

///////////////////////////////////////////////////////////////////////////////
///
/// \brief Client for the coinAIche game.
///
///////////////////////////////////////////////////////////////////////////////
class ClientGUI : public QWidget, Client
{
    Q_OBJECT
public:
    ///////////////////////////////////////////////////////////////////////////
    /// \brief      Constructor.
    ////////////////////////////////////////////////////////////////////////////////////////
    ClientGUI(string const& name, string const& server = DEFAULT_SERVER);

    ///////////////////////////////////////////////////////////////////////////
    /// \brief      Destructor.
    ///////////////////////////////////////////////////////////////////////////
    ~ClientGUI() = default;

private slots:
    void southPositionSlot();
    void westPositionSlot();
    void northPositionSlot();
    void eastPositionSlot();
    void cardSlot();
    
    void sendBidSlot();
    void sendPassSlot();
    void sendCoincheSlot();
    
private:
    void positionCallback() override;
    void auctionCallback(string const& msg) override;
    void playCallback(string const& msg) override;

    void welcomeCallback(string const& msg) override;
    void newPlayerCallback(string const& msg) override;
    void startCallback() override;
    void handCallback(string const& msg) override;
    void indexCallback(string const& msg) override;
    void lastBidCallback(string const& msg) override;
    void lastCardCallback(string const& msg) override;
    void trickCallback(string const& msg) override;
    void contractCallback(string const& msg) override;
    void gamePointsCallback(string const& msg) override;
    void scoresCallback(string const& msg, bool const& isEnd = false) override;

    void coincheCallback(string const& msg) override;
    void beloteCallback() override;

    void tryAgainCallback(string const& msg = "") override;
    void genericErrorCallback(string const& msg = "") override;

    void setAuctionsVisibility(bool const visibility);
    void setAuctionsFrameVisibility(bool const visibility);

    Ui::ClientGUI *ui;

    PlayerId firstPlayer_;

    array<QLabel*, N_PLAYERS> names_;
    array<QLabel*, N_PLAYERS> playedCards_;
    array<QLabel*, N_PLAYERS> lastFold_;
    array<QLabel*, N_PLAYERS> beers_;
    array<QLabel*, N_PLAYERS> bubbles_;
    array<QLabel*, N_PLAYERS> bidValues_;
    array<QLabel*, N_PLAYERS> bidColors_;
    array<QLabel*, N_PLAYERS> coinches_;
    array<QClickableLabel*, 8> cards_;
    array<QFrame*, 8> cardMasks_;

    QClickableLabel cardClicked_;
    GUIPositionWindow* winPosition_;
};

#endif