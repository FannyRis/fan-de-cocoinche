#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QWidget>
#include <QLabel>

class QClickableLabel : public QLabel
{
    Q_OBJECT
public:
    QClickableLabel(QWidget *parent=0);
    ~QClickableLabel() {}


    bool getJustGotClicked();
    void resetJustGotClicked();
    void setIsClickable(bool newIsClickableValue);


signals:
    void clicked();
    
protected:
    void mouseReleaseEvent(QMouseEvent*); 

private:
    bool justGotClicked_;
    bool isClickable_;
};

#endif // CLICKABLELABEL_H
