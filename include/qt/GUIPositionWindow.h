#ifndef GUIPOSITIONWINDOW_H
#define GUIPOSITIONWINDOW_H

#include <QApplication>
#include <QLabel>
#include <QWidget>
#include <QFormLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QTableWidget>

#include "Player.h"

class GUIPositionWindow : public QWidget
{
    Q_OBJECT

public:
    GUIPositionWindow(QWidget* client, array<Player, 4> players);
    virtual ~GUIPositionWindow();

    PlayerId getPosition();
    array<QPushButton*, 4> getButtons() { return buttons_; };


private:
    QVBoxLayout* layout_;
    array<QPushButton*, 4> buttons_;
};

#endif  // GUIPOSITIONWINDOW_H
