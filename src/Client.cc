#include "Client.h"

void* doRead(Client* clientIn)
{
    return clientIn->readLoop();
}

void* doWrite(Client* clientIn)
{
    return clientIn->writeLoop();
}


Client::Client(string const& name, string const& server)
    : run_signal_(true)
    , id_(N_PLAYERS)
    , players_()
    , contract_()
    , hand_()
    , scores_({0}, {0})
    , belote_(false)
    , waitingForPosition_(false)
    , waitingForAuction_(false)
    , waitingForCoinche_(false)
    , waitingForPlay_(false)
    , client_socket_()
{
    server_ = server;
    client_socket_.connectWithTimeout(server_.c_str(), DEFAULT_PORT, 5, 0);
    client_socket_.sendString(name);
    readThread_ = std::thread(doRead, this);
}


Client::~Client()
{
    if (run_signal_)
    {
        client_socket_.close();
        run_signal_ = false;
    }
}


void Client::joinWriteThread()
{
    std::thread writeThread(doWrite, this);
    writeThread.join();
}


void Client::waitForEnd()
{
    readThread_.join();
    client_socket_.close();
}


void Client::logout()
{
    run_signal_ = false;
}


void* Client::readLoop()
{
    string msg, tag;
    while (run_signal_)
    {
        if (client_socket_.canRecv(SLEEP_DELAY))
        {
            msg = client_socket_.recvString();
            tag = msg.substr(0, 2);
            if (tag == POSITION_TAG)
            {
                positionCallback();
            }
            else if (tag == AUCTION_TAG)
            {
                auctionCallback(msg.substr(2));
            }
            else if (tag == PLAY_TAG)
            {
                playCallback(msg.substr(2));
            }
            else if (tag == WELCOME_TAG)
            {
                welcomeCallback(msg.substr(2));
            }
            else if (tag == NEW_PLAYER_TAG)
            {
                newPlayerCallback(msg.substr(2));
            }
            else if (tag == ALL_PLAYERS_CONNECTED_TAG)
            {
                startCallback();
            }
            else if (tag == HAND_TAG)
            {
                handCallback(msg.substr(2));
            }
            else if (tag == INDEX_HAND_TAG)
            {
                indexCallback(msg.substr(2));
            }
            else if (tag == LAST_BID_TAG)
            {
                lastBidCallback(msg.substr(2));
            }
            else if (tag == LAST_CARD_TAG)
            {
                lastCardCallback(msg.substr(2));
            }
            else if (tag == TRICK_TAG)
            {
                trickCallback(msg.substr(2));
            }
            else if (tag == CONTRACT_TAG)
            {
                contractCallback(msg.substr(2));
            }
            else if (tag == GAME_POINTS_TAG)
            {
                gamePointsCallback(msg.substr(2));
                belote_ = false;
            }
            else if (tag == SCORES_TAG)
            {
                scoresCallback(msg.substr(2));
            }
            else if (tag == COINCHE_TAG)
            {
                coincheCallback(msg.substr(2));
            }
            else if (tag == BELOTE_TAG)
            {
                beloteCallback();
                belote_ = true;
            }
            else if (tag == END_TAG)
            {
                scoresCallback(msg.substr(2), true);
                exitCallback();
            }
            else if (tag == EXIT_TAG)
            {
                exitCallback();
            }
            else if (tag == INVALID_POSITION_TAG)
            {
                tryAgainCallback("(Enter N for north, if you don't care)\n");
            }
            else if (tag == INVALID_CARD_TAG)
            {
                tryAgainCallback("(Enter the index corresponding to the card with a B to announce belote or rebelote)\n");
            }
            else if (tag == INVALID_BID_TAG)
            {
                tryAgainCallback("Invalid bid!");
            }
            else if (tag == INVALID_AUCTION_MESSAGE_TAG)
            {
                tryAgainCallback("(At your turn: pass or 80-S for 80 at spades)\n Else: toctoc to coinche\n");
            }
            else if (tag == INVALID_COINCHE_TAG)
            {
                genericErrorCallback("Your are not allowed to coinche!");
            }
            else if (tag == INVALID_BELOTE_TAG)
            {
                genericErrorCallback("Invalid belote!");
            }
            else if (tag == INCOMPLETE_BELOTE_TAG)
            {
                genericErrorCallback("The belote was not completed!");
                belote_ = false;
            }
            else
            {
                // cout << msg << endl;
            }
            usleep(SLEEP_DELAY);
        }
    }
    void* r;
    return r;
}


void* Client::writeLoop()
{
    string msg;
    char outBuff[OUT_BUFFER_SIZE] = {0};

    while (run_signal_)
    {
        cin.getline(outBuff, OUT_BUFFER_SIZE);

        msg = (string)outBuff;

        if (!msg.empty())
        {
            if ( regex_match(msg, regex(EXIT_REGEX)) )
            {
                msg = EXIT_TAG;
            }
            else
            {
                if (waitingForPosition_)
                {
                    waitingForPosition_ = false;
                }
                else if (waitingForPlay_)
                {
                    waitingForPlay_ = false;
                }
                if (waitingForCoinche_)
                {
                    if (regex_match(msg, regex(COINCHE_REGEX)))
                    {
                        msg = COINCHE_TAG;
                    }
                }
                if (waitingForAuction_)
                {
                    if (regex_match(msg, regex(CLIENT_BID_REGEX)))
                    {
                        msg = toBidRegex(msg);
                    }
                    waitingForAuction_ = false;
                }
            }
            client_socket_.sendString(msg);
            
            msg.clear();
            memset(outBuff, 0, OUT_BUFFER_SIZE);
        }
    }
    void* r;
    return r;
}


void Client::positionCallback()
{
    string answer;
    cout << availablePositionsToString() + "Choose your position !" << endl;
    waitingForPosition_ = true;
}


void Client::auctionCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg);
    if (player == id_)
    {
        cout << "Your turn to bid!\n";
        waitingForAuction_ = true;
    }
}


void Client::playCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg);
    if (player == id_)
    {
        cout << "Your turn ! Which card do you play?\n";
        waitingForPlay_ = true;
    }
}


void Client::welcomeCallback(string const& msg)
{
    cout << "Welcome to FanDeCoCoinche " << endl;
    string s = msg.substr(0, msg.size() - (NAME_LENGTH + 1));
    if (!s.size())
    {
        cout << "You are the first one to connect!" << endl;
    }
    else if (regex_match(s, regex(PLAYERS_REGEX)))
    {
        int index = 0;
        while (index + NAME_LENGTH < s.size())
        {
            string name = s.substr(index + 1, NAME_LENGTH);
            PlayerId id = stringToPlayerId(s.substr(index, 1));
            players_[id] = Player(id, name);
            cout << name << " is playing " << playerIdStrings[id] << endl;
            index += 1 + NAME_LENGTH;
        }
    }
}


void Client::newPlayerCallback(string const& msg)
{
    if (id_ == N_PLAYERS)
    {
        id_ = stringToPlayerId(msg.substr(0, 1));
        string name = msg.substr(1, NAME_LENGTH);
        players_[id_] = Player(id_, name);
        cout << name << ", you will be playing " << playerIdStrings[id_] << endl;
    }
    else
    {
        PlayerId id = stringToPlayerId(msg.substr(0, 1));
        string name = msg.substr(1, NAME_LENGTH);
        players_[id] = Player(id, name);
        cout << name << " is playing " << playerIdStrings[id] << endl;
    }
}


void Client::startCallback()
{
    PlayerId partner = id_;
    partner += 2;
    cout << "All the players are now connected. You are playing with " << players_[partner].getName() << endl;
    cout << "                                                             " << endl;
    cout << "-------------------------------------------------------------" << endl;
    cout << "|                    L E T ' S   P L A Y  !                 |" << endl;
    cout << "-------------------------------------------------------------\n " << endl;
}


void Client::handCallback(string const& msg)
{
    hand_ = Deck(msg);
    if (hand_.size())
    {
        cout << "Your hand is:" << endl;
        cout << hand_.toPrettyString() << endl;
    }
    else
    {
        cout << "Your hand is empty!\n " << endl;
    }
}


void Client::indexCallback(string const& msg)
{
    cout << "Cards you can play:" << endl;
    cout << hand_.toPrettyStringIndex(stringToMask(msg)) << endl;
}


void Client::lastBidCallback(string const& msg)
{
    Bid bid(msg);
    cout << "                    ";
    if (bid.player == id_)
    {
        cout << "You" << bid.toPrettyString() << endl;
    }
    else
    {
        cout << players_[bid.player].getName() << bid.toPrettyString() << endl;
    }

    waitingForCoinche_ = true;
}


void Client::lastCardCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg.substr(0, 1));
    cout << "                    ";
    Card card(msg.substr(1));
    if (player == id_)
    {
        cout << "You played " << card.toPrettyIcon() << endl;
    }
    else
    {
        cout << players_[player].getName() << " played " << card.toPrettyIcon() << endl;
    }
}


void Client::trickCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg.substr(0, 1));
    if (player == id_)
    {
        cout << "You won the trick\n " << endl;
    }
    else
    {
        cout << players_[player].getName() << " won the trick\n " << endl;
    }
    cout << "-------------------------------------------------------------\n " << endl;
}


void Client::contractCallback(string const& msg)
{
    waitingForCoinche_ = false;

    contract_ = Bid(msg);
    cout << "The contract is:" << endl;
    cout << contract_.contractString() << endl;
    cout << "-------------------------------------------------------------\n " << endl;
}


void Client::gamePointsCallback(string const& msg)
{
    Scores points;
    points.addEntry(msg);
    cout << "The contract was:" << endl;
    cout << contract_.contractString() << endl;
    cout << "Points of each team ";
    if (belote_)
    {
        cout << "(without the belote) ";
    }
    cout << ": " << endl;
    cout << points.toPrettyString() << endl;
}


void Client::scoresCallback(string const& msg, bool const& isEnd)
{
    scores_.addEntry(msg);
    cout << "Total scores won by each team :" << endl;
    if (isEnd)
    {
        cout << scores_.toEndString() << endl;
    }
    else
    {
        cout << scores_.toPrettyString() << endl;
    }
    cout << "-------------------------------------------------------------" << endl;
    cout << "-------------------------------------------------------------\n " << endl;
}


void Client::coincheCallback(string const& msg)
{
    cout << "Toc toc ! Coinché !\n " << endl;
}


void Client::beloteCallback()
{
    cout << "                    ";
    if (belote_)
    {
        cout << "Re belote !\n " << endl;
    }
    else
    {
        cout << "Belote !\n " << endl;
    }
}


void Client::exitCallback()
{
    cout << "Closing socket..." << endl;
    logout();
}


void Client::tryAgainCallback(string const& msg)
{
    cout << msg + "Try again..." << endl;
}


void Client::genericErrorCallback(string const& msg)
{
    cout << "Error! " + msg << endl;
}


string Client::availablePositionsToString() const
{
    vector<PlayerId> positionsTaken;
    for (int i = 0; i < 4; i++)  // For each player
    {
        if (players_[i].getId() != N_PLAYERS)
        {
            positionsTaken.push_back(players_[i].getId());
        }
    }

    string result = "You can play :\n";
    for (int i = 0; i < 4; i++)  // For each position
    {
        PlayerId position = static_cast<PlayerId>(i);
        auto it = std::find(positionsTaken.begin(), positionsTaken.end(), position);
        if (it == positionsTaken.end())
        {
            result += "- " + playerIdStrings[position];
            PlayerId partner = position;
            partner += 2;
            it = std::find(positionsTaken.begin(), positionsTaken.end(), partner);
            if (it != positionsTaken.end())
            {
                result += " with " + players_[*it].getName();
            }
            result += "\n";
        }
    }
    return result;
}


string Client::toBidRegex(string const& stringIn) const
{
    Bid bid;
    if (stringIn == "pass")
    {
        bid = Bid(id_);
    }
    else
    {
        int value = atoi(stringIn.substr(0, stringIn.find("-")).c_str());
        Color color = stringToColor(stringIn.substr(stringIn.find("-") + 1));
        bid = Bid(value, color, id_);
    }
    return bid.toString();
}


vector<bool> Client::stringToMask(string const& stringIn)
{
    vector<bool> mask;
    for (int i = 0; i < stringIn.size(); i++)
    {
        mask.push_back(stringIn.substr(i, 1) == "1");
    }
    return mask;
}
