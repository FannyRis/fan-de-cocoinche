///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Implementation of QClickablelabel, a QLabel that can be clicked!
///
/// \copyright C&F
///
///////////////////////////////////////////////////////////////////////////////////////////////////
#include "qt/QClickableLabel.h"
#include <iostream>

QClickableLabel::QClickableLabel(QWidget* parent)
    : QLabel(parent)
    , justGotClicked_(false)
    , isClickable_(true)
{

}

void QClickableLabel::mouseReleaseEvent(QMouseEvent *)
{
    justGotClicked_ = true;
    std::cout << "CLICKED !!! " << std::endl;
    emit clicked();
}


bool QClickableLabel::getJustGotClicked()
{
    return justGotClicked_;
}

void QClickableLabel::resetJustGotClicked()
{
    justGotClicked_ = false;
}

void QClickableLabel::setIsClickable(bool newIsClickableValue)
{
    isClickable_ = newIsClickableValue;
}

