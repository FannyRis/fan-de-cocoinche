#include "qt/GUIPositionWindow.h"
#include "Player.h"

using namespace std;

GUIPositionWindow::GUIPositionWindow(QWidget* client, array<Player, 4> players)
    : QWidget()
{
    // Creation of the potential position buttons
    buttons_[0] = new QPushButton("South");
    buttons_[0]->setFixedWidth(100);
    buttons_[0]->setVisible(false);

    buttons_[1] = new QPushButton("West");
    buttons_[1]->setFixedWidth(100);
    buttons_[1]->setVisible(false);

    buttons_[2] = new QPushButton("North");
    buttons_[2]->setFixedWidth(100);
    buttons_[2]->setVisible(false);

    buttons_[3] = new QPushButton("East");
    buttons_[3]->setFixedWidth(100);
    buttons_[3]->setVisible(false);

    QWidget* widgetButtons = new QWidget;
    QHBoxLayout* hlayout = new QHBoxLayout(widgetButtons);
    hlayout->addWidget(buttons_[0]);
    hlayout->addWidget(buttons_[1]);
    hlayout->addWidget(buttons_[2]);
    hlayout->addWidget(buttons_[3]);


    // process input msg
    vector<PlayerId> positionsTaken;
    for (int i = 0; i < 4; i++)  // For each player
    {
        if (players[i].getId() != N_PLAYERS)
        {
            positionsTaken.push_back(players[i].getId());
        }
    }

    string result = "You can play :\n";
    for (int i = 0; i < 4; i++)  // For each position
    {
        PlayerId position = static_cast<PlayerId>(i);
        auto it = std::find(positionsTaken.begin(), positionsTaken.end(), position);
        if (it == positionsTaken.end())
        {
            // this position is available, its corresponding button must be visible
            buttons_[i]->setVisible(true);

            result += "- " + playerIdStrings[position];
            PlayerId partner = position;
            partner += 2;
            it = std::find(positionsTaken.begin(), positionsTaken.end(), partner);
            if (it != positionsTaken.end())
            {
                result += " with " + players[*it].getName();
            }
            result += "\n";
        }
    }

    QLabel* information = new QLabel(result.c_str());

    // layout_ consist of a vertical layout with first the text and then the buttons
    layout_ = new QVBoxLayout(this);

    this->setWindowTitle("Pick a side");
    this->setFixedWidth(600);

    layout_->addWidget(information);
    layout_->addWidget(widgetButtons);
}

GUIPositionWindow::~GUIPositionWindow()
{
}
