#include "qt/ClientGUI.h"
#include "ui_client.h"

ClientGUI::ClientGUI(string const& name, string const& server)
    : QWidget(nullptr)
    , Client(name, server)
    , ui(new Ui::ClientGUI)
    , firstPlayer_(N_PLAYERS)
{
    ui->setupUi(this);

    ui->scoreLeft->setText("0");
    ui->scoreRight->setText("0");

    names_[0] = ui->playerName;
    names_[1] = ui->leftPlayer;
    names_[2] = ui->topPlayer;
    names_[3] = ui->rightPlayer;

    lastFold_[0] = ui->foldBottom;
    lastFold_[1] = ui->foldLeft;
    lastFold_[2] = ui->foldTop;
    lastFold_[3] = ui->foldRight;

    beers_[0] = ui->bottomBeer;
    beers_[1] = ui->leftBeer;
    beers_[2] = ui->topBeer;
    beers_[3] = ui->rightBeer;

    playedCards_[0] = ui->bottomCard;
    playedCards_[1] = ui->leftCard;
    playedCards_[2] = ui->topCard;
    playedCards_[3] = ui->rightCard;

    bubbles_[0] = ui->bottomBubble;
    bubbles_[1] = ui->leftBubble;
    bubbles_[2] = ui->topBubble;
    bubbles_[3] = ui->rightBubble;

    bidValues_[0] = ui->bottomValue;
    bidValues_[1] = ui->leftValue;
    bidValues_[2] = ui->topValue;
    bidValues_[3] = ui->rightValue;

    bidColors_[0] = ui->bottomColor;
    bidColors_[1] = ui->leftColor;
    bidColors_[2] = ui->topColor;
    bidColors_[3] = ui->rightColor;

    coinches_[0] = ui->bottomCoinche;
    coinches_[1] = ui->leftCoinche;
    coinches_[2] = ui->topCoinche;
    coinches_[3] = ui->rightCoinche;

    for (uint32_t i = 0; i < N_PLAYERS; i++)
    {
        names_[i]->setVisible(false);
        playedCards_[i]->setVisible(false);
        lastFold_[i]->setVisible(false);
        beers_[i]->setVisible(false);
        bubbles_[i]->setVisible(false);
        bidColors_[i]->setVisible(false);
        bidValues_[i]->setVisible(false);
        coinches_[i]->setVisible(false);
    }

    cards_[0] = ui->card0;
    cards_[1] = ui->card1;
    cards_[2] = ui->card2;
    cards_[3] = ui->card3;
    cards_[4] = ui->card4;
    cards_[5] = ui->card5;
    cards_[6] = ui->card6;
    cards_[7] = ui->card7;

    cardMasks_[0] = ui->cardMask0;
    cardMasks_[1] = ui->cardMask1;
    cardMasks_[2] = ui->cardMask2;
    cardMasks_[3] = ui->cardMask3;
    cardMasks_[4] = ui->cardMask4;
    cardMasks_[5] = ui->cardMask5;
    cardMasks_[6] = ui->cardMask6;
    cardMasks_[7] = ui->cardMask7;
    for (uint32_t i = 0; i < cards_.size(); i++)
    {
        cards_[i]->setVisible(false);
        cardMasks_[i]->setVisible(false);
    }

    for (uint32_t i = 0; i < cards_.size(); i++)
    {
        connect(cards_[i], SIGNAL(clicked()), this, SLOT(cardSlot()));
    }

    ui->auctionsFrame->setVisible(false);
    ui->auctionsCoinche->setVisible(false);

    connect(ui->auctionsOk, SIGNAL(clicked()), this, SLOT(sendBidSlot()));
    connect(ui->auctionsPass, SIGNAL(clicked()), this, SLOT(sendPassSlot()));
    connect(ui->auctionsCoinche, SIGNAL(clicked()), this, SLOT(sendCoincheSlot()));
}


void ClientGUI::positionCallback()
{
    winPosition_ = new GUIPositionWindow(this, players_);
    winPosition_->show();

    connect(winPosition_->getButtons()[0], SIGNAL(clicked()), this, SLOT(southPositionSlot()));
    connect(winPosition_->getButtons()[1], SIGNAL(clicked()), this, SLOT(westPositionSlot()));
    connect(winPosition_->getButtons()[2], SIGNAL(clicked()), this, SLOT(northPositionSlot()));
    connect(winPosition_->getButtons()[3], SIGNAL(clicked()), this, SLOT(eastPositionSlot()));
}


void ClientGUI::auctionCallback(string const& msg)
{
    std::cout << "auctionsCB" << std::endl;

    PlayerId player = stringToPlayerId(msg);
    if (player == id_)
    {
        setAuctionsVisibility(true);
        ui->auctionsFrame->setVisible(true);
    }
    player += 4 - id_;
    beers_[player]->setVisible(true);
}


void ClientGUI::playCallback(string const& msg)
{
    std::cout << "playCB" << std::endl;

    PlayerId player = stringToPlayerId(msg);
    player += 4 - id_;
    beers_[player]->setVisible(true);
}


void ClientGUI::welcomeCallback(string const& msg)
{
    string s = msg.substr(0, msg.size() - (NAME_LENGTH + 1));
    if (s.size() and regex_match(s, regex(PLAYERS_REGEX)))
    {
        int index = 0;
        while (index + NAME_LENGTH < s.size())
        {
            string name = s.substr(index + 1, NAME_LENGTH);
            PlayerId id = stringToPlayerId(s.substr(index, 1));
            players_[id] = Player(id, name);
            names_[id]->setText(name.c_str());
            names_[id]->setVisible(true);
            index += 1 + NAME_LENGTH;
        }
    }
}


void ClientGUI::newPlayerCallback(string const& msg)
{
    if (id_ == N_PLAYERS)
    {
        id_ = stringToPlayerId(msg.substr(0, 1));
        string name = msg.substr(1, NAME_LENGTH);
        players_[id_] = Player(id_, name);
        for (int i = 0; i < N_PLAYERS; i++)
        {
            PlayerId player = static_cast<PlayerId>(i);
            player += 4 - id_;
            if (players_[i].getId() != N_PLAYERS)
            {
                names_[player]->setText(players_[i].getName().c_str());
                names_[player]->setVisible(true);
                if (players_[i].getId() == SOUTH)
                {
                    firstPlayer_ = player;
                }
            }
            else
            {
                names_[player]->setVisible(false);
            }
        }
    }
    else
    {
        PlayerId id = stringToPlayerId(msg.substr(0, 1));
        string name = msg.substr(1, NAME_LENGTH);
        players_[id] = Player(id, name);
        PlayerId player = id;
        player += 4 - id_;
        names_[player]->setText(players_[id].getName().c_str());
        names_[player]->setVisible(true);
    }
}


void ClientGUI::startCallback()
{
}


void ClientGUI::handCallback(string const& msg)
{
    hand_ = Deck(msg);
    for (uint32_t i = 0; i < 8; i++)
    {
        cards_[i]->setVisible(false);
        cardMasks_[i]->setVisible(false);
    }

    for (uint32_t i = 0; i < hand_.size(); i++)
    {
        std::string s = CARD_PATH + hand_[i].toPath();
        QString cardPath(s.c_str());
        cards_[i]->setPixmap({cardPath});
        cards_[i]->setVisible(true);
    }
}


void ClientGUI::indexCallback(string const& msg)
{
    vector<bool> index = stringToMask(msg);
    for (uint32_t i = 0; i < 8; i++)
    {
        cards_[i]->setVisible(false);
        cardMasks_[i]->setVisible(false);
    }

    for (uint32_t i = 0; i < hand_.size(); i++)
    {
        std::string s = CARD_PATH + hand_[i].toPath();
        QString cardPath(s.c_str());
        cards_[i]->setPixmap({cardPath});
        cards_[i]->setVisible(true);
        cardMasks_[i]->setVisible(!index[i]);
    }
}


void ClientGUI::lastBidCallback(string const& msg)
{
    std::cout << "lastBid CB" << std::endl;

    Bid bid(msg);
    PlayerId player = bid.player;
    PlayerId partner = id_;
    partner += 2;
    bool possibleCoinche = (player != id_) and (player != partner) and (!bid.pass);

    player += 4 - id_;
    bubbles_[player]->setVisible(true);
    if (bid.pass)
    {
        bidValues_[player]->setText("Pass");
        bidValues_[player]->setVisible(true);
        bidColors_[player]->setVisible(false);
    }
    else
    {
        string colorPath = BACKGROUND_PATH + colorStrings[bid.color] + ".png";
        bidColors_[player]->setPixmap({colorPath.c_str()});
        bidColors_[player]->setVisible(true);
        bidValues_[player]->setText(to_string(bid.value).c_str());
        bidValues_[player]->setVisible(true);
    }

    setAuctionsVisibility(false);
    ui->auctionsFrame->setVisible(possibleCoinche);
    ui->auctionsCoinche->setVisible(possibleCoinche);

    beers_[player]->setVisible(false);
    player++;
    beers_[player]->setVisible(true);
}


void ClientGUI::lastCardCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg.substr(0, 1));
    player += 4 - id_;
    Card card(msg.substr(1));

    std::string s = CARD_PATH + card.toPath();
    QString cardPath(s.c_str());
    playedCards_[player]->setPixmap({cardPath});
    playedCards_[player]->setVisible(true);
    beers_[player]->setVisible(false);
}


void ClientGUI::trickCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg.substr(0, 1));
    player += 4 - id_;

    for (uint32_t i = 0; i < N_PLAYERS; i++)
    {
        string s = BACKGROUND_PATH;
        s += "back.png";
        QString cardPath(s.c_str());
        lastFold_[i]->setPixmap(*playedCards_[i]->pixmap());
        lastFold_[i]->setVisible(true);
        playedCards_[i]->setVisible(false);
    }
}


void ClientGUI::contractCallback(string const& msg)
{
    for (int i = 0; i < N_PLAYERS; i++)
    {
        bubbles_[i]->setVisible(false);
        bidValues_[i]->setVisible(false);
        bidColors_[i]->setVisible(false);
        beers_[i]->setVisible(false);
    }
    setAuctionsVisibility(false);
    ui->auctionsFrame->setVisible(false);
    ui->auctionsCoinche->setVisible(false);

    contract_ = Bid(msg);
    PlayerId player = contract_.player;
    player += 4 - id_;
    bubbles_[player]->setVisible(true);
    bidValues_[player]->setVisible(true);
    bidColors_[player]->setVisible(true);
    bool coinched = (contract_.coincheCoefficient > 1);
    beers_[firstPlayer_]->setVisible(true);
}


void ClientGUI::scoresCallback(string const& msg, bool const& isEnd)
{
    if (regex_match(msg, regex(SCORE_REGEX)))
    {
        string s1 = msg.substr(0, msg.find("-"));
        string s2 = msg.substr(msg.find("-") + 1);

        TeamId team = playersTeam[id_];
        if (team == NORTH_SOUTH)
        {
            ui->scoreLeft->setText(s1.c_str());
            ui->scoreRight->setText(s2.c_str());
        }
        else
        {
            ui->scoreRight->setText(s1.c_str());
            ui->scoreLeft->setText(s2.c_str());
        }
    }
}


void ClientGUI::coincheCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg);
    player += 4 - id_;
    coinches_[player]->setVisible(true);
}


void ClientGUI::beloteCallback()
{
    // Empty
}


void ClientGUI::gamePointsCallback(string const& msg)
{
    contract_ = Bid();
}


void ClientGUI::tryAgainCallback(string const& msg)
{
    // Empty
}


void ClientGUI::genericErrorCallback(string const& msg)
{
    // Empty
}

// IMPLEMENTATION OF THE SLOTS //
void ClientGUI::southPositionSlot()
{
    winPosition_->close();
    client_socket_.sendString("S");
}

void ClientGUI::westPositionSlot()
{
    winPosition_->close();
    client_socket_.sendString("W");
}

void ClientGUI::northPositionSlot()
{
    winPosition_->close();
    client_socket_.sendString("N");
}

void ClientGUI::eastPositionSlot()
{
    winPosition_->close();
    client_socket_.sendString("E");
}


void ClientGUI::sendBidSlot()
{
    std::cout << "sendBidSlot" << std::endl;
    string bid_str = ui->auctionsValue->currentText().toStdString();
    bid_str += "-" + ui->auctionsColor->currentText().toStdString().substr(0, 1);
    client_socket_.sendString(toBidRegex(bid_str));
}


void ClientGUI::sendPassSlot()
{
    std::cout << "sendPassSlot" << std::endl;
    client_socket_.sendString(toBidRegex("pass"));
}


void ClientGUI::sendCoincheSlot()
{
    std::cout << "sendCoincheSlot" << std::endl;
    client_socket_.sendString(COINCHE_TAG);
}


void ClientGUI::cardSlot()
{
    for (uint32_t i = 0; i < cards_.size(); i++)
    {
        if (cards_[i]->getJustGotClicked())
        {
            cards_[i]->resetJustGotClicked();
            client_socket_.sendString(to_string(i));
        }
    }
}


void ClientGUI::setAuctionsVisibility(bool const visibility)
{
    ui->auctionsOk->setVisible(visibility);
    ui->auctionsPass->setVisible(visibility);
    ui->auctionsValue->setVisible(visibility);
    ui->auctionsColor->setVisible(visibility);
    ui->auctionsText->setVisible(visibility);
}