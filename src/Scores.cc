///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Implementation of Scores
///
/// \copyright C&F
///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "Scores.h"

Scores::Scores()
    : array<vector<int>, 2>()
{
    //Empty
}

Scores::Scores(array<vector<int>, 2> const& scoresIn)
    : array<vector<int>, 2>(scoresIn)
{
    //Empty
}

Scores::Scores(vector<int> const& scoresTeamNS, vector<int> const& scoresTeamEW)
    : array<vector<int>, 2>({scoresTeamNS, scoresTeamEW})
{
    // Empty
}

void Scores::addEntry(TeamId const& teamIn, int const& points, int const& otherPoints)
{
    TeamId team = teamIn;
    at(team).push_back(points);
    team++;
    at(team).push_back(otherPoints);
}

void Scores::addEntry(array<int, 2> const& points)
{
    at(0).push_back(points[0]);
    at(1).push_back(points[1]);
}


void Scores::addEntry(std::string const& stringIn)
{
    if (regex_match(stringIn, std::regex(SCORE_REGEX)))
    {
        int s1 = atoi(stringIn.substr(0, stringIn.find("-")).c_str());
        at(0).push_back(s1);

        int s2 = atoi(stringIn.substr(stringIn.find("-") + 1).c_str());
        at(1).push_back(s2);
    }
    else
    {
        cerr << "Scores : unable to add entry ! (Msg = " << stringIn << ") " << endl;
    }
}

array<int, 2> Scores::getTotals() const
{
    array<int, 2> a = {0, 0};
    int size = at(0).size();
    if (size != at(1).size())
    {
        cerr << "Scores : Error ! the sizes of the two columns don't match !" << std::endl;
        return a;
    }
    a[0] = at(0)[size - 1];
    a[1] = at(1)[size - 1];

    return a;
}


int Scores::getTotal(TeamId const& team) const
{
    return getTotals()[team];
}


array<int, 2> Scores::getLastScores() const
{
    int size = at(0).size();
    if (size < 2)
    {
        return getTotals();
    }
    array<int, 2> a;
    if (size != at(1).size())
    {
        cerr << "Scores : Error ! the sizes of the two columns don't match !" << std::endl;
        return a;
    }
    a[0] = at(0)[size - 1] - at(0)[size - 2];
    a[1] = at(1)[size - 1] - at(1)[size - 2];
    return a;
}


int Scores::getLastScore(TeamId const& team) const
{
    return getLastScores()[team];
}


string Scores::totalToString() const
{
    array<int, 2> totals = getTotals();
    return to_string(totals[0]) + "-" + to_string(totals[1]);
}


string Scores::lastToString() const
{
    array<int, 2> lasts = getLastScores();
    return to_string(lasts[0]) + "-" + to_string(lasts[1]);
}


string Scores::toPrettyString() const
{
    int size = at(0).size();
    if (size != at(1).size())
    {
        cerr << "Scores : Error ! the sizes of the two columns don't match !" << std::endl;
        return "";
    }

    string scores = "|----|----|\n";
    scores += "| NS | EW |\n";
    scores += "|----|----|\n";
    for (int i = 0; i < size; i++)
    {
        scores += "|" + padString(to_string(at(0)[i]), SCORE_LENGTH, true);
        scores += "|" + padString(to_string(at(1)[i]), SCORE_LENGTH, true) + "|\n";
    }
    scores += "|----|----|\n";
    return scores;
}


string Scores::toEndString() const
{
    string result = toPrettyString();
    result += teamIdStrings[getLeadingTeam()] + " team won with a score of " + to_string(getHighestScore()) + "\n";
    return result;
}


TeamId Scores::getLeadingTeam() const
{
    array<int, 2> totals = getTotals();
    if (totals[NORTH_SOUTH] == totals[EAST_WEST])
    {
        return N_TEAMS;
    }
    if (totals[NORTH_SOUTH] < totals[EAST_WEST])
    {
        return EAST_WEST;
    }
    else
    {
        return NORTH_SOUTH;
    }
}


int Scores::getHighestScore() const
{
    return getTotal(getLeadingTeam());
}
