///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Implementation of Motor
///
/// \copyright C&F
///
///////////////////////////////////////////////////////////////////////////////////////////////////
#include "Motor.h"

using namespace std;

Motor::Motor()
    : currentPlayer_(SOUTH)
    , firstPlayer_(SOUTH)
    , firstTrickPlayer_(SOUTH)
    , scores_({0}, {0})
    , deck_()
    , teamDecks_()
    , players_()
    , contract_()
    , numberOfPass_(0)
    , hasValidBid_(false)
    , stopAuctions_(false)
    , numberOfTrick_(0)
    , belote_()
    , state_(WAIT_FOR_PLAYERS)
{
    deck_.create();
    deck_.shuffleDeck();
    server_socket_.listen(DEFAULT_PORT);
}


Motor::~Motor()
{
    sendAllClients(EXIT_TAG);
    server_socket_.close();
}


void Motor::run(uint32_t nAI)
{
    cout << "[INFO] " << stateStrings[state_] << endl;
    while (state_ != END)
    {
        switch (state_)
        {
        case WAIT_FOR_PLAYERS:
            acceptClient();
            cout << "[INFO] " << currentPlayer_ << " players connected." << endl;
            if (currentPlayer_ == N_PLAYERS - nAI)
            {
                // if nAI has not a default value
                if ( nAI != N_PLAYERS )
                {
                    // then we complete the players with nAI AI
                    for (uint32_t i = 0; i < nAI; i++)
                    {
                        AIpids_.push_back(fork());
                        if (AIpids_[i] == 0)
                        {
                            // child process
                            string c("./FanDeCoCoinche -a " + to_string(i));
                            system(c.c_str());
                            state_ = END;
                            break;
                        }
                        else if (AIpids_[i] > 0)
                        {
                            acceptClient();
                            cout << "[INFO] " << currentPlayer_ << " players connected." << endl;
                        }
                        else
                        {
                            // fork failed
                            printf("fork() failed!\n");
                        }
                    }
                }
                if (state_ != END)
                {
                    array<Player, 4> playersTemp;
                    for (int i = 0; i < 4; i++)
                    {
                        playersTemp[i] = getPlayer(static_cast<PlayerId>(i));
                    }
                    players_ = playersTemp;
                    sendAllClients(ALL_PLAYERS_CONNECTED_TAG);
                    state_ = DEAL;
                    cout << "[INFO] " << stateStrings[state_] << endl;
                }
            }
            break;
        case DEAL:
            contract_ = Bid();
            numberOfPass_ = 0;
            hasValidBid_ = false;
            stopAuctions_ = false;
            currentPlayer_ = firstPlayer_;
            deal();
            for (uint32_t i = 0; i < N_PLAYERS; i++)
            {
                sendClient((PlayerId)i, HAND_TAG, players_[i].getHand().toString());
            }
            currentPlayer_ = firstPlayer_;
            state_ = AUCTIONS;
            cout << "[INFO] " << stateStrings[state_] << endl;
            break;
        case AUCTIONS:
            runAuctions();
            if (state_ == END)
            {}
            else
            {
                if (stopAuctions_)
                {
                    sendAllClients(CONTRACT_TAG, contract_.toString());
                    for (uint32_t i = 0; i < N_PLAYERS; i++)
                    {
                        Deck hand = players_[i].getHand();
                        hand.sortDeck(contract_.color);
                        players_[i].setHand(hand);
                        sendClient((PlayerId)i, HAND_TAG, players_[i].getHand().toString());
                    }
                    firstTrickPlayer_ = firstPlayer_;
                    state_ = PLAY;
                    cout << "[INFO] " << stateStrings[state_] << endl;
                }
                else if (numberOfPass_ == 4)
                {
                    firstPlayer_++;
                    deck_ = Deck();
                    for (uint32_t i = 0; i < 4; i++)
                    {
                        deck_.addDeck(players_[currentPlayer_].takeAllCards());
                        currentPlayer_++;
                    }
                    state_ = DEAL;
                    cout << "[INFO] " << stateStrings[state_] << endl;
                }
                else
                {
                    currentPlayer_++;
                }
            }
            break;
        case PLAY:
            playTrick();
            if (state_ == END)
            {}
            else
            {
                numberOfTrick_ += 1;

                if (numberOfTrick_ == 8)
                {
                    computeScores();
                    numberOfTrick_ = 0;

                    if (scores_.getHighestScore() < MAX_SCORE)
                    {
                        deck_.addDeck(teamDecks_[0].emptyDeck());
                        deck_.addDeck(teamDecks_[1].emptyDeck());
                        belote_.empty();
                        firstPlayer_++;
                        currentPlayer_ = firstPlayer_;
                        sendAllClients(SCORES_TAG, scores_.totalToString());
                        state_ = DEAL;
                        cout << "[INFO] " << stateStrings[state_] << endl;
                    }
                    else
                    {
                        sendAllClients(END_TAG, scores_.totalToString());
                        state_ = END;
                        cout << "[INFO] " << stateStrings[state_] << endl;
                    }
                }
            }
            break;
        default:
            break;
        }
    }
    // remember to kill the potential child processes
    
    for (uint32_t i = 0; i < nAI; i++)
    {
        kill(AIpids_[i], SIGTERM);
    }
}


void Motor::sendAllClients(string const& tag, string const& msg)
{
    for (int i = 0; i < 4; i++)
    {
        if (players_[i].getId() != N_PLAYERS)
        {
            sendClient(static_cast<PlayerId>(i), tag, msg);
        }
    }
}


void Motor::sendClient(PlayerId const& player, string const& tag, string const& msg)
{
    queue_mutex_.lock();
    players_[player].getSocket().sendString(tag + msg);
    queue_mutex_.unlock();
    usleep(MEDIUM_SLEEP_DELAY);
}


void Motor::acceptClient()
{

    TCPSocket client = server_socket_.accept();
    string port = to_string(client.getPort());

    cout << "New player connecting... (IP: " + client.getAddress() + ")" << endl;

    string username = client.recvString();
    if (!username.empty())
    {
        cout << username + " connected (IP: " + client.getAddress() + ")" << endl;
        server_socket_.monitor(client);

        usleep(LONG_SLEEP_DELAY);

        players_[currentPlayer_] = Player({N_PLAYERS, username, client});
        sendClient(currentPlayer_, WELCOME_TAG, playersToString());
        PlayerId position = getPosition(currentPlayer_);
        currentPlayer_++;
    }
    else
    {
        cout << "Connection failed..." << endl;
        client.close();
    }
}


PlayerId Motor::getPosition(PlayerId const& player)
{

    PlayerId position;
    string msg;
    bool correctPosition = false;
    vector<PlayerId> availablePositions = getAvailablePositions();

    if (availablePositions.size() == 1 or availablePositions.size() == 4)
    {
        position = availablePositions[0];
        players_[player].setId(position);
        sendAllClients(NEW_PLAYER_TAG, to_string(players_[player].getId()) + players_[player].getName());
        return position;
    }

    while (!correctPosition)
    {
        sendClient(player, POSITION_TAG);
        msg = players_[player].getSocket().recvString();
        cout << msg << endl;
        if (msg == " ")
        {
            position = availablePositions[0];
            players_[currentPlayer_].setId(position);
            sendAllClients(NEW_PLAYER_TAG, to_string(players_[currentPlayer_].getId()) + players_[currentPlayer_].getName());
            correctPosition = true;
        }
        else if (regex_match(msg, regex(POSITION_REGEX)))
        {
            position = stringToPlayerId(msg);
            auto it = std::find(availablePositions.begin(), availablePositions.end(), position);
            if (it != availablePositions.end())
            {
                players_[player].setId(position);
                sendAllClients(NEW_PLAYER_TAG, to_string(position) + players_[player].getName());
                correctPosition = true;
            }
        }
        else
        {
            sendClient(player, INVALID_POSITION_TAG);
        }
    }
    return position;
}


void Motor::deal()
{
    deck_.split();

    PlayerId player = firstPlayer_;

    for (uint32_t i = 0; i < 4; i++)
    {
        players_[player].dealCards(deck_.removeDeck(3));
        player++;
    }
    for (uint32_t i = 0; i < 4; i++)
    {
        players_[player].dealCards(deck_.removeDeck(2));
        player++;
    }
    for (uint32_t i = 0; i < 4; i++)
    {
        players_[player].dealCards(deck_.removeDeck(3));
        player++;
    }

    for (uint32_t i = 0; i < 4; i++)
    {
        Deck hand = players_[i].getHand();
        hand.sortDeck();
        players_[i].setHand(hand);
    }
}


void Motor::runAuctions()
{
    Bid currentBid = getAuction(currentPlayer_);
    if (currentBid.pass)
    {
        numberOfPass_ += 1;
    }
    else
    {
        contract_ = currentBid;
        hasValidBid_ = true;
        numberOfPass_ = 0;
    }

    if (hasValidBid_ and numberOfPass_ == 3)
    {
        stopAuctions_ = true;
    }
}

Bid Motor::getAuction(PlayerId player)
{
    Bid bid;
    bool correctBid = false;
    string msg;

    sendAllClients(AUCTION_TAG, to_string(player));
    while (!correctBid)
    {
        TCPSocket client;
        Message msg = getFirstMessage();
        if (msg.content == EXIT_TAG)
        {
            sendAllClients(EXIT_TAG);
            server_socket_.close();
            state_ = END;
        }
        else if (msg.content == COINCHE_TAG)
        {
            if (isCoincheValid(msg.sender))
            {
                contract_ = Bid(contract_, 2);
                sendAllClients(COINCHE_TAG, to_string(msg.sender));
                correctBid = true;
                stopAuctions_ = true;
            }
            else
            {
                sendClient(msg.sender, INVALID_COINCHE_TAG);
            }
        }
        else if (regex_match(msg.content, regex(BID_REGEX)) and msg.sender == player)
        {
            bid = Bid(msg.content);
            if (isBidValid(bid))
            {
                sendAllClients(LAST_BID_TAG, bid.toString());
                correctBid = true;
            }
            else
            {
                sendClient(player, INVALID_BID_TAG);
                sendAllClients(AUCTION_TAG, to_string(player));
            }
        }
        else
        {
            sendClient(msg.sender, INVALID_AUCTION_MESSAGE_TAG);
            if (msg.sender == player)
            {
                sendAllClients(AUCTION_TAG, to_string(player));
            }
        }
    }
    return bid;
}


Message Motor::getFirstMessage()
{
    Message msg;
    while (true)
    {
        vector<TCPSocket> events = server_socket_.getEvents(SLEEP_DELAY);
        for (auto& client : events)
        {
            msg.content = client.recvString();
            if (!msg.content.empty())
            {
                msg.sender = clientToPlayerId(client);
                return msg;
            }
        }
        events.empty();
        usleep(SLEEP_DELAY);
    }
}


bool Motor::isBidValid(Bid const& bid) const
{
    if (hasValidBid_)
    {
        return bid.isValid(contract_);
    }
    return bid.isValid();
}


bool Motor::isCoincheValid(PlayerId const& player) const
{
    return hasValidBid_ and !numberOfPass_ and playersTeam[player] != playersTeam[contract_.player];
}


bool Motor::isBeloteValid(PlayerId const& player, Card const& card) const
{
    if (card.color != contract_.color)
    {
        return false;
    }
    if (card.figure != QUEEN and card.figure != KING)
    {
        return false;
    }
    if (belote_.player != N_PLAYERS and player != belote_.player)
    {
        return false;
    }
    return true;
}


PlayerId Motor::clientToPlayerId(TCPSocket const& client) const
{
    PlayerId player = N_PLAYERS;
    for (int i = 0; i < 4; i++)
    {
        if (players_[i].getSocket() == client)
        {
            player = players_[i].getId();
        }
    }
    return player;
}


Player Motor::getPlayer(PlayerId const& idIn) const
{
    for (int i = 0; i < 4; i++)
    {
        if (players_[i].getId() == idIn)
        {
            return players_[i];
        }
    }
    cerr << "No player with this Id in players_" << endl;
    return Player();
}


vector<PlayerId> Motor::getAvailablePositions() const
{
    vector<PlayerId> positions;
    for (int p = 0; p < 4; p++)  // for each position
    {
        bool found = false;
        for (int i = 0; i < 4; i++)  // For each player
        {
            if (static_cast<PlayerId>(p) == players_[i].getId())
            {
                found = true;
            }
        }
        if (!found)
        {
            positions.push_back(static_cast<PlayerId>(p));
        }
    }
    return positions;
}


void Motor::playTrick()
{
    Deck cards;
    currentPlayer_ = firstTrickPlayer_;
    for (uint8_t i = 0; i < 4; i++)
    {
        vector<bool> handMask = getHandMask(cards);
        cards.push_back(getCard(currentPlayer_, handMask));
        currentPlayer_++;
    }

    sleep(2);

    uint32_t index = cards.getWinningCardIndex(contract_.color);
    firstTrickPlayer_ += index;

    sendAllClients(TRICK_TAG, to_string(firstTrickPlayer_));
    teamDecks_[playersTeam[firstTrickPlayer_]].addDeck(cards);
}


Card Motor::getCard(PlayerId const& player, vector<bool> const& handMask)
{
    sendClient(player, INDEX_HAND_TAG, maskToString(handMask));
    sendAllClients(PLAY_TAG, to_string(player));

    Card card;
    bool correctCard(false);
    string msg;

    while (!correctCard)
    {
        msg = players_[player].getSocket().recvString();
        
        if (msg == EXIT_TAG)
        {
            sendAllClients(EXIT_TAG);
            server_socket_.close();
            state_ = END;
        }
        else if (regex_match(msg, regex(PLAY_REGEX)))
        {
            int index = atoi(msg.substr(0, 1).c_str());
            if (handMask[index])
            {
                card = players_[player].getHand()[index];
                players_[player].takeCard(card);
                sendClient(currentPlayer_, HAND_TAG, players_[currentPlayer_].getHand().toString());
                sendAllClients(LAST_CARD_TAG, to_string(currentPlayer_) + card.toString());
                correctCard = true;
            }
            else
            {
                sendClient(player, INVALID_CARD_TAG);
                sendAllClients(PLAY_TAG, to_string(player));
            }

            // Check belote
            if (msg.size() == 2)
            {
                if (isBeloteValid(player, card))
                {
                    if (belote_.player == N_PLAYERS)
                    {
                        belote_.player = player;
                    }
                    else
                    {
                        belote_.complete = true;
                    }
                    sendAllClients(BELOTE_TAG);
                }
                else
                {
                    sendClient(player, INVALID_BELOTE_TAG);
                }
            }
        }
        else
        {
            sendClient(player, INVALID_CARD_TAG);
            sendAllClients(PLAY_TAG, to_string(player));
        }
    }
    return card;
}


string Motor::maskToString(vector<bool> const& mask)
{
    string s = "";
    for (int i = 0; i < mask.size(); i++)
    {
        if (mask[i])
        {
            s += "1";
        }
        else
        {
            s += "0";
        }
    }
    return s;
}

string Motor::playersToString()
{
    string s;
    for (int i = 0; i < players_.size(); i++)
    {
        s += to_string(players_[i].getId()) + players_[i].getName();
    }
    return s;
}

void Motor::computeScores()
{
    TeamId team = playersTeam[contract_.player];
    uint32_t declarerPoints = teamDecks_[team].count(contract_.color);
    if (playersTeam[firstTrickPlayer_] == team)
    {
        declarerPoints += 10;
    }
    Scores points;
    points.addEntry(team, declarerPoints, 162 - declarerPoints);
    sendAllClients(GAME_POINTS_TAG, points.totalToString());
    uint32_t belotePoints = 0;
    if (playersTeam[belote_.player] == team)
    {
        if (!belote_.complete)
        {
            sendAllClients(INCOMPLETE_BELOTE_TAG);
        }
        else if (declarerPoints < contract_.value and declarerPoints + 20 >= contract_.value)
        {
            declarerPoints += 20;
        }
        else
        {
            belotePoints = 20;
        }
    }
    if (declarerPoints < contract_.value)
    {
        team++;
        if (playersTeam[belote_.player] == team)
        {
            if (belote_.complete)
            {
                belotePoints = 20;
            }
            else
            {
                sendAllClients(INCOMPLETE_BELOTE_TAG);
            }
        }
    }
    array<int, 2> totals = scores_.getTotals();
    totals[team] += contract_.value * contract_.coincheCoefficient + belotePoints;
    scores_.addEntry(totals);
}


vector<bool> Motor::getHandMask(Deck const& cards) const
{
    Deck hand = players_[currentPlayer_].getHand();
    vector<bool> mask(hand.size(), false);
    for (int i = 0; i < hand.size(); i++)
    {
        if (isCardAllowed(cards, hand, i))
        {
            mask[i] = true;
        }
    }
    return mask;
}


bool Motor::isPartnerWinning(Deck const& cards) const
{
    if (cards.size() > 1)
    {
        int partnerCardIndex = cards.size() - 2;
        int index = cards.getWinningCardIndex(contract_.color);

        if (index == partnerCardIndex)
        {
            return true;
        }
    }
    return false;
}


Card Motor::getHighestTrump(Deck const& cards) const
{
    Card highestTrump = {N_COLORS, SEVEN};
    Color trump = contract_.color;
    for (int i = 0; i < cards.size(); i++)
    {
        if (cards[i].color == trump and highestTrump.smallerThan(cards[i], trump, trump))
        {
            highestTrump = cards[i];
        }
    }
    return highestTrump;
}


bool Motor::isCardAllowed(Deck const& cards, Deck const& hand, int const& index) const
{
    if (!cards.size())
    {
        return true;
    }

    Color trump = contract_.color;
    Color askedColor = cards[0].color;
    Card card = hand[index];
    Card lastTrump = getHighestTrump(cards);
    bool partnerIsWinning = isPartnerWinning(cards);
    bool hasOtherTrump = false;
    bool hasBiggerTrump = false;
    bool hasColor = false;

    for (int i = 0; i < hand.size(); i++)
    {
        if (hand[i].color == trump and i != index)
        {
            hasOtherTrump = true;
            if (lastTrump.smallerThan(hand[i], trump, askedColor))
            {
                hasBiggerTrump = true;
            }
        }
        else if (hand[i].color == askedColor)
        {
            hasColor = true;
        }
    }

    if (askedColor != trump)
    {
        if (card.color == askedColor)
        {
            return true;
        }
        if (hasColor)
        {
            return false;
        }
        if (card.color == trump)
        {
            if (!hasBiggerTrump)
            {
                return true;
            }
            if (lastTrump.figure != N_FIGURES and card.smallerThan(lastTrump, trump, askedColor))
            {
                return false;
            }
            return true;
        }
        if (hasOtherTrump and !partnerIsWinning)
        {
            return false;
        }
        return true;
    }
    if (card.color == trump)
    {
        if (!hasBiggerTrump)
        {
            return true;
        }
        if (card.smallerThan(lastTrump, trump, askedColor))
        {
            return false;
        }
        return true;
    }
    if (hasOtherTrump)
    {
        return false;
    }
    return true;
}