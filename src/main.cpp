#include "Client.h"
#include "ClientAI.h"
#include "Motor.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << " [ --motor,-m  [NUMBER OF AI] / --player,-p [USERNAME] [CUSTOM IP ADDRESS] / --ai,-a [USERNAME] ]" << std::endl;
        return 1;
    }

    std::vector<std::string> args(argv, argv + argc);
    if (args[1] == "--motor" || args[1] == "-m")
    {
        uint32_t nAI(N_PLAYERS);
        if (argc > 2)
        {
            nAI = atoi(args[2].c_str());
            if (nAI == 0)
            {
                nAI = N_PLAYERS;
            }
        }
        Motor motor;
        cout << "Motor created." << endl;
        motor.run(nAI);
    }
    else if (args[1] == "--player" || args[1] == "-p")
    {
        std::string username;
        std::string serverAddress;

        // do we have a player name?
        if (argc > 2)
        {
            username = args[2];
        }
        if (argc > 3)
        {
            serverAddress = args[3];
        }

        if (username.empty())
        {
            cout << "Enter username: ";
            cin >> username;
        }

        if (serverAddress.empty())
        {
            serverAddress = DEFAULT_SERVER;
        }

        Client client(padString(username, NAME_LENGTH), serverAddress);
        client.joinWriteThread();
        client.waitForEnd();
    }
    else if (args[1] == "--ai" || args[1] == "-a")
    {
        std::string username;
        // do we have a player name?
        if (argc > 2)
        {
            username = args[2];
        }

        if (username.empty())
        {
            cout << "Enter username: ";
            cin >> username;
        }

        ClientAI clientAI(padString("AI-" + username, NAME_LENGTH));
        clientAI.waitForEnd();
    }
    else
    {
        std::cerr << "Usage: " << argv[0] << " [ --motor,-m  [NUMBER OF AI] / --player,-p [USERNAME] [CUSTOM IP ADDRESS] / --ai,-a [USERNAME] ]" << std::endl;
        return 1;
    }

    return 0;
}
