#include "ClientAI.h"

ClientAI::ClientAI(string const& name, string const& server)
    : Client(name, server)
    , contract_()
    , bids_()
    , cards_()
    , mask_()
    , lastBid_()
    , playedCards_()
{
    // Empty
}


void ClientAI::positionCallback()
{
    client_socket_.sendString(" ");
}


void ClientAI::auctionCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg);
    if (player == id_)
    {
        Bid bid = doBid();
        sleep(3);
        client_socket_.sendString(bid.toString());
    }
}


void ClientAI::playCallback(string const& msg)
{
    PlayerId player = stringToPlayerId(msg);
    if (player == id_)
    {
        uint16_t index = playCard();
        string answer = to_string(index);
        sleep(3);
        client_socket_.sendString(answer);
    }
}

void ClientAI::welcomeCallback(string const& msg)
{
    string s = msg.substr(0, msg.size() - (NAME_LENGTH + 1));
    if (s.size() and regex_match(s, regex(PLAYERS_REGEX)))
    {
        int index = 0;
        while (index + NAME_LENGTH < s.size())
        {
            string name = s.substr(index + 1, NAME_LENGTH);
            PlayerId id = stringToPlayerId(s.substr(index, 1));
            players_[id] = Player(id, name);
            index += 1 + NAME_LENGTH;
        }
    }
}


void ClientAI::newPlayerCallback(string const& msg)
{
    if (id_ == N_PLAYERS)
    {
        id_ = stringToPlayerId(msg.substr(0, 1));
        string name = msg.substr(1, NAME_LENGTH);
        players_[id_] = Player(id_, name);
    }
    else
    {
        PlayerId id = stringToPlayerId(msg.substr(0, 1));
        string name = msg.substr(1, NAME_LENGTH);
        players_[id] = Player(id_, name);
    }
}


void ClientAI::startCallback()
{
    // Empty
}


void ClientAI::handCallback(string const& msg)
{
    hand_ = Deck(msg);
    players_[id_].setHand(Deck(msg));
}


void ClientAI::indexCallback(string const& msg)
{
    Client::indexCallback(msg);
    mask_ = stringToMask(msg);
}


void ClientAI::lastBidCallback(string const& msg)
{
    bids_.push_back(Bid(msg));
}


void ClientAI::lastCardCallback(string const& msg)
{
    cards_.push_back(Card(msg.substr(1)));
}


void ClientAI::trickCallback(string const& msg)
{
    for (int i = 0; i < cards_.size(); i++)
    {
        playedCards_.push_back(cards_[i]);
    }
    cards_ = Deck();
}


void ClientAI::contractCallback(string const& msg)
{
    contract_ = Bid(msg);
    if (contract_.player == id_)
    {
        players_[id_].setIsDeclarer(true);
    }
}


void ClientAI::scoresCallback(string const& msg, bool const& isEnd)
{
    scores_.addEntry(msg);
}


void ClientAI::coincheCallback(string const& msg)
{
    // Empty
}


void ClientAI::beloteCallback()
{
    // Empty
}


void ClientAI::gamePointsCallback(string const& msg)
{
    playedCards_ = {};
    contract_ = Bid();
}


void ClientAI::tryAgainCallback(string const& msg)
{
    // Empty
}


void ClientAI::genericErrorCallback(string const& msg)
{
    // Empty
}


uint16_t ClientAI::playCard()
{
    vector<int> indexes = getIndexes();
    if (indexes.size() == 1)
    {
        return indexes[0];
    }

    Color trump = contract_.color;
    Deck cards = players_[id_].getHand().getCards(indexes);
    Deck winningCards = cards.getWinningCards(playedCards_, trump);
    int index = 0;
    if (!cards_.size())  // First one playing
    {
        if (playersTeam[contract_.player] == players_[id_].getTeamId())  // J'attaque
        {
            if (playedCards_.numberOf(trump) + cards.numberOf(trump) < 8)  // Tous les atouts ne sont pas tombés
            {
                if (winningCards.numberOf(trump))  // J'ai un atout maître
                {
                    return indexes[cards.index(winningCards.getBest(trump, true))];
                }
                if (cards.numberOf(trump))  // J'ai un atout
                {
                    return indexes[cards.index(cards.getSmallest(trump, true))];
                }
            }
        }
        if (winningCards.size() > winningCards.numberOf(trump))  // J'ai une carte SA maître
        {
            return indexes[cards.index(winningCards.getBestNot(trump))];
        }
        if (cards.size() > cards.numberOf(trump))  // J'ai une carte SA
        {
            return indexes[cards.index(cards.getSmallestNot(trump))];
        }
        else if (winningCards.numberOf(trump))  // J'ai un atout maître
        {
            return indexes[cards.index(winningCards.getBest(trump, true))];
        }
    }
    else  // Je ne suis pas la première à jouer
    {
        Color askedColor = cards_[0].color;
        if (askedColor != trump)  // carte pas atout
        {
            if (winningCards.numberOf(askedColor) and !cards_.numberOf(trump))  // J'ai une carte maître à la couleur et personne n'a coupé
            {
                return indexes[cards.index(winningCards.getBest(askedColor))];
            }
            else if (cards_.getWinningCardIndex(trump) == cards_.size() - 2)  // Mon partenaire est maître
            {
                if (cards.numberOf(askedColor))  // J'ai de la couleur
                {
                    if (cards.isWinning(cards_[cards_.getWinningCardIndex(trump)], playedCards_, trump))  // Sa carte est maître
                    {
                        return indexes[cards.index(cards.getBest(askedColor))];
                    }
                    return indexes[cards.index(cards.getSmallest())];
                }
                if (playersTeam[contract_.player] != players_[id_].getTeamId())  // je suis en défense
                {
                    if (cards.numberOf(trump) == 1 and !winningCards.numberOf(trump))  // J'ai un seul atout non maître
                    {
                        if (cards.getSmallest(trump, true).getTrumpValue() >= 10)  // Il vaut 10 points ou plus
                        {
                            return indexes[cards.index(cards.getSmallest(trump, true))];
                        }
                    }
                }
                if (cards.size() > cards.numberOf(trump))  // J'ai une autre couleur que l'atout
                {
                    if (winningCards.size() == winningCards.numberOf(trump))  // Je n'ai pas de carte maître à SA
                    {
                        return indexes[cards.index(cards.getBestNot(trump))];
                    }
                    else  // J'ai une carte maître à SA
                    {
                        return indexes[cards.index(cards.getSmallestNot(trump))];
                    }
                }
            }
            if (!cards.numberOf(askedColor))  // Je n'ai pas de la couleur
            {
                if (cards.numberOf(trump))  // Je coupe
                {
                    if (!winningCards.numberOf(trump) and cards.numberOf(trump) == 2)  // J'ai deux atouts non maîtres
                    {
                        return indexes[cards.index(cards.getBest(trump, true))];
                    }
                }
                else  // Je pisse
                {
                    return indexes[cards.index(cards.getSmallest())];
                }
            }
        }
    }
    return indexes[0];
}


vector<int> ClientAI::getIndexes() const
{
    vector<int> indexes;
    for (int i = 0; i < mask_.size(); i++)
    {
        if (mask_[i])
        {
            indexes.push_back(i);
        }
    }
    return indexes;
}

Bid ClientAI::doBid()
{
    Bid bid = evaluateHand();

    int numberOfBids = bids_.size();

    if (numberOfBids > 0)
    {
        Bid lastValidBid = getLastValidBid();

        if (numberOfBids > 1)
        {
            Bid lastPartnerBid = bids_[numberOfBids - 2];
            if (!lastPartnerBid.pass)
            {
                if ((!lastBid_.pass) and (lastBid_.color == lastPartnerBid.color))
                {
                    bid = Bid(id_);
                }
                else
                {
                    Bid bidOn = evaluateOn(lastPartnerBid);
                    if (bid.value < bidOn.value)
                    {
                        bid = bidOn;
                    }
                }
            }
        }
        if (!bid.isValid(lastValidBid))
        {
            bid = Bid(id_);
        }
    }

    lastBid_ = bid;
    return bid;
}


Bid ClientAI::evaluateHand()
{
    Bid bid(id_);
    std::array<Deck, 4> colorDecks = players_[id_].getHand().getColorDecks();

    array<int, 4> aces = {0, 0, 0, 0};
    for (int c = 0; c < 4; c++)
    {
        for (int i = 0; i < 4; i++)
        {
            if (i != c and colorDecks[i].hasCard({(Color)i, ACE}))
            {
                aces[c] += 1;
            }
        }
    }

    for (int c = 0; c < 4; c++)
    {
        Deck deck = colorDecks[c];
        Color trump = static_cast<Color>(c);
        int bidValue = 0;

        bool hasJack = deck.hasCard({trump, JACK});
        bool hasNine = deck.hasCard({trump, NINE});
        bool hasBelote = players_[id_].hasBelote(trump);

        switch (deck.size())
        {
        case 3:
        {
            if (hasJack and hasNine)
            {
                bidValue = 80 + 10 * aces[c];
            }
            else if ((hasJack or hasJack) and aces[c] >= 1)
            {
                bidValue = 80 + 10 * (aces[c] - 1);
            }
            break;
        }
        case 4:
        {
            if (hasJack and hasNine)
            {
                bidValue = 100 + 10 * aces[c];
            }
            else if ((hasJack or hasJack) and aces[c] >= 1)
            {
                bidValue = 90 + 10 * (aces[c] - 1);
            }
            break;
        }
        case 5:
        {
            if (hasJack and hasNine and aces[c] == 3)
            {
                bidValue = 500;
            }
            else if (hasJack and hasNine)
            {
                bidValue = 110 + 10 * aces[c];
            }
            else if ((hasJack or hasJack) and aces[c] >= 1)
            {
                bidValue = 90 + 10 * (aces[c] - 1);
            }
            break;
        }
        case 6:
            if (hasJack and hasNine and aces[c] == 2)
            {
                bidValue = 500;
            }
            else if (hasJack)
            {
                bidValue = 120 + 10 * aces[c];
            }
            else
            {
                bidValue = 100 + 10 * aces[c];
            }
        case 7:
        {
            if (hasJack and aces[c] == 1)
            {
                bidValue = 500;
            }
            else if (hasJack)
            {
                bidValue = 140;
            }
            else
            {
                bidValue = 120;
            }
            break;
        }
        case 8:
            bidValue = 500;
            break;
        default:
            break;
        }

        if (hasBelote and 90 < bidValue)
        {
            bidValue += 20;
        }

        if (bid.value < bidValue)
        {
            bid = Bid(bidValue, static_cast<Color>(c), id_);
        }
    }

    return bid;
}


Bid ClientAI::evaluateOn(Bid const& otherBid)
{
    Color trump = otherBid.color;
    array<Deck, N_COLORS> colorDecks = players_[id_].getHand().getColorDecks();
    Deck deck = colorDecks[trump];

    uint32_t aces = 0;
    bool hasJack = deck.hasCard({trump, JACK});
    bool hasNine = deck.hasCard({trump, NINE});
    bool hasBelote = players_[id_].hasBelote(trump);
    int numberTrumps = deck.size();
    for (int c = 0; c < N_COLORS; c++)
    {
        if (c != trump)
        {
            auto it = std::find(colorDecks[c].begin(), colorDecks[c].end(), Card((Color)c, ACE));
            if (it != colorDecks[c].end())
            {
                aces++;
            }
        }
    }

    int value = 20 * ((int)hasBelote + (int)hasJack) + 10 * (aces + (int)hasNine + max(0, numberTrumps - 2) - (int)(numberTrumps == 0));

    Bid bid(id_);
    if (otherBid.value == 80 and !hasJack and !(hasNine and numberTrumps > 1))
    {
        return bid;
    }

    if (value > 0)
    {
        bid = Bid(otherBid.value + value, trump, id_);
    }
    return bid;
}

Bid ClientAI::getLastValidBid()
{
    Bid bid;

    if (bids_.size())
    {
        for (int i = 0; i < bids_.size(); i++)
        {
            if (!bids_[i].pass)
            {
                bid = bids_[i];
            }
        }
    }
    return bid;
}