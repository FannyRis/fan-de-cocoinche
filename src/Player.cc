///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Implementation of Player
///
/// \copyright C&F
///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "Player.h"

Player::Player()
    : id_(N_PLAYERS)
    , name_("")
    , socket_()
    , hand_()
    , isDeclarer_(false)
{
    //Empty
}


Player::Player(PlayerId const& idIn, string const& nameIn)
    : id_(idIn)
    , name_(nameIn)
    , socket_()
    , hand_()
    , isDeclarer_(false)
{
    //Empty
}


Player::Player(PlayerId const& idIn, std::string const& nameIn, TCPSocket const& socketIn)
    : id_(idIn)
    , name_(nameIn)
    , socket_(socketIn)
    , hand_()
    , isDeclarer_(false)
{
    //Empty
}


void Player::dealCards(Deck const& cards)
{
    hand_.addDeck(cards);
}


Deck Player::takeAllCards()
{
    return hand_.removeDeck(8);
}


void Player::takeCard(Card const& card)
{
    hand_.removeCard(card);
}


void Player::setId(PlayerId const& idIn)
{
    id_ = idIn;
}


void Player::setName(std::string const& nameIn)
{
    name_ = nameIn;
}


void Player::setSocket(TCPSocket const& socketIn)
{
    socket_ = socketIn;
}


void Player::setHand(Deck const& handIn)
{
    hand_ = handIn;
}


void Player::setIsDeclarer(bool const& isDeclarerIn)
{
    isDeclarer_ = isDeclarerIn;
}


PlayerId Player::getId() const
{
    return id_;
}


std::string Player::getName() const
{
    return name_;
}


TCPSocket Player::getSocket() const
{
    return socket_;
}


Deck Player::getHand() const
{
    return hand_;
}

TeamId Player::getTeamId() const
{
    return playersTeam[id_];
}

bool Player::isDeclarer() const
{
    return isDeclarer_;
}

bool Player::hasBelote(Color const& trump) const
{
    array<Deck, 4> colorDecks = hand_.getColorDecks();
    Deck deck = colorDecks[trump];
    if (deck.hasCard({trump, QUEEN}) and deck.hasCard({trump, KING}))
    {
        return true;
    }
    return false;
}
