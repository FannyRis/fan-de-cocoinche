///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// \brief Implementation of Deck
///
/// \copyright C&F
///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "Deck.h"

#include <ctime>
using namespace std;

Deck::Deck()
    : vector<Card>()
{
    //Empty
}

Deck::Deck(vector<Card> const& cards)
    : vector<Card>(cards)
{
    //Empty
}


Deck::Deck(string const& stringIn)
    : vector<Card>()
{
    if (stringIn.size() and regex_match(stringIn, std::regex(DECK_REGEX)))
    {
        for (int index = 0; index + 1 < stringIn.size(); index += 2)
        {
            push_back(Card(stringIn.substr(index, 2)));
        }
    }
}


void Deck::create()
{
    for (uint32_t i = 0; i < N_COLORS; i++)
    {
        for (uint32_t j = 0; j < N_FIGURES; j++)
        {
            Card card(static_cast<Color>(i), static_cast<Figure>(j));
            push_back(card);
        }
    }
}


void Deck::shuffleDeck()
{
    random_device rd;
    mt19937 g(rd());

    shuffle(begin(), end(), g);
}


void Deck::split()
{
    srand(time(0));
    uint32_t i = (rand() % (size() - 4)) + 2;
    vector<Card> head(begin(), begin() + i);
    erase(begin(), begin() + i);
    insert(end(), head.begin(), head.end());
}


uint32_t Deck::count(Color const trump) const
{
    uint32_t result = 0;
    for (auto it = begin(); it != end(); ++it)
    {
        Card card = *it;
        if (card.color == trump)
        {
            result += card.getTrumpValue();
        }
        else
        {
            result += card.getNoTrumpValue();
        }
    }
    return result;
}


int Deck::numberOf(Color const& color) const
{
    return getColorDecks()[color].size();
}


bool Deck::hasCard(Card const& card) const
{
    auto it = find(begin(), end(), card);
    if (it == end())
    {
        return false;
    }
    else
    {
        return true;
    }
}


int Deck::index(Card const& card) const
{
    int index = -1;
    for (int i = 0; i < size(); i++)
    {
        if (at(i) == card)
        {
            index = i;
        }
    }
    return index;
}


Card Deck::getSmallest(Color const& color, bool const& isTrump) const
{
    Card card = {N_COLORS, N_FIGURES};
    for (int i = 0; i < size(); i++)
    {
        if (at(i).color == color)
        {
            if (!isTrump and at(i) < card)
            {
                card = at(i);
            }
            if (isTrump and Card::smallerTrump(at(i), card))
            {
                card = at(i);
            }
        }
        if (color == N_COLORS and at(i) < card)
        {
            card = at(i);
        }
    }
    return card;
}


Card Deck::getBest(Color const& color, bool const& isTrump) const
{
    Card card = {N_COLORS, SEVEN};
    for (int i = 0; i < size(); i++)
    {
        if (at(i).color == color)
        {
            if ((isTrump and Card::smallerTrump(card, at(i))) or (!isTrump and card <= at(i)))
            {
                card = at(i);
            }
        }
    }
    if (card.color == N_COLORS)
    {
        card.figure = N_FIGURES;
    }
    return card;
}


Card Deck::getSmallestNot(Color const& color) const
{
    Card card;
    for (int i = 0; i < size(); i++)
    {
        if (at(i).color != color and at(i) < card)
        {
            card = at(i);
        }
    }
    return card;
}


Card Deck::getBestNot(Color const& color) const
{
    Card card = {N_COLORS, SEVEN};
    for (int i = 0; i < size(); i++)
    {
        if (at(i).color != color and at(i) >= card)
        {
            card = at(i);
        }
    }
    if (card.color == N_COLORS)
    {
        card.figure = N_FIGURES;
    }
    return card;
}


void Deck::removeCard(Card const card)
{
    auto it = find(begin(), end(), card);
    if (it == end())
    {
        cout << "Unable to remove card : card not found" << endl;
    }
    else
    {
        erase(it);
    }
}


Deck Deck::removeDeck(uint32_t const numberOfCards)
{
    int index = size() - numberOfCards;
    Deck result(vector<Card>(begin() + index, end()));
    erase(begin() + index, end());

    return result;
}

Deck Deck::emptyDeck()
{
    return removeDeck(size());
}


void Deck::addDeck(Deck const& deck)
{
    insert(end(), deck.begin(), deck.end());
}


void Deck::sortDeck(Color const& trump)
{
    array<Deck, N_COLORS> colorDecks = getColorDecks();
    *this = Deck();
    for (int i = 0; i < N_COLORS; i++)
    {
        if (trump == i)
        {
            sort(colorDecks[i].begin(), colorDecks[i].end(), Card::smallerTrump);
        }
        else
        {
            sort(colorDecks[i].begin(), colorDecks[i].end());
        }

        addDeck(colorDecks[i]);
    }
}


array<Deck, N_COLORS> Deck::getColorDecks() const
{
    array<Deck, 4> colorDecks;
    for (int i = 0; i < size(); i++)
    {
        Color color = at(i).color;
        colorDecks[color].push_back(at(i));
    }
    return colorDecks;
}


uint32_t Deck::getWinningCardIndex(Color const trump) const
{
    uint32_t index = 0;
    Color askedColor = at(0).color;

    for (uint32_t i = 1; i < size(); i++)
    {
        if (at(index).smallerThan(at(i), trump, askedColor))
        {
            index = i;
        }
    }
    return index;
}


string Deck::toString() const
{
    string result = "";

    for (uint32_t i = 0; i < size(); i++)
    {
        result += at(i).toString();
    }
    return result;
}

string Deck::toPrettyString() const
{
    string result = "| ";

    for (uint32_t i = 0; i < size(); i++)
    {
        result += at(i).toPrettyIcon() + " | ";
    }
    return result;
}

string Deck::toPrettyStringIndex(vector<bool> const& mask) const
{
    if (mask.size() != size())
    {
        return "Error ! Mask size and deck size must match";
    }

    string result = "";
    for (uint32_t i = 0; i < size(); i++)
    {
        if (mask[i])
        {
            result += to_string(i) + " - " + at(i).toPrettyIcon() + "\n";
        }
    }
    return result;
}

bool Deck::isWinning(Card const& card, Deck const& playedCards, Color const& trump) const
{
    Color color = card.color;
    Deck deck = playedCards.getColorDecks()[color];
    for (int i = 0; i < N_FIGURES; i++)
    {
        Card other = {(Figure)i, color};
        if (!hasCard(other) and !deck.hasCard(other))
        {
            if (color == trump and Card::smallerTrump(card, other))
            {
                return false;
            }
            else if (color != trump and card < other)
            {
                return false;
            }
        }
    }
    return true;
}

Deck Deck::getWinningCards(Deck const& playedCards, Color const& trump) const
{
    Deck cards;
    for (int i = 0; i < size(); i++)
    {
        if (isWinning(at(i), playedCards, trump))
        {
            cards.push_back(at(i));
        }
    }
    return cards;
}


Deck Deck::getCards(vector<int> const& indexes) const
{
    Deck cards;
    for (int i = 0; i < indexes.size(); i++)
    {
        cards.push_back(at(i));
    }
    return cards;
}