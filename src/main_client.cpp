#include "qt/ClientGUI.h"
#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    std::string username = "Fanny";
    std::string serverAddress = DEFAULT_SERVER;

    ClientGUI clientGUI(padString(username, NAME_LENGTH), serverAddress);
    clientGUI.show();
    
    return a.exec();
}
